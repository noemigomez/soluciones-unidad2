#include <stdio.h>
#include <stdlib.h>	
#include <time.h>

void nuevo(char nucleotido, int i, int j){
	if(i!=0 && j==0){
		printf("\n");
	}
	printf("|%c|", nucleotido);
}

void mutaciones_vecinos(char base, int vecinos, int i, int j){
	if(vecinos==3){
		switch(base){
			case 'A':
			base='T';
			break;
			
			case 'C':
			base='G';
			break;
			
			case 'G':
			base='C';
			break;
			
			case 'T':
			base='A';
			break;
			}
		}
	else{
		base="ACGT"[rand()%4];
	}
	nuevo(base, i, j);
}

void cuantos_vecinos(char base, char vecino, int dimension, int i, int j){
	int vecinoA, vecinoC, vecinoG, vecinoT, veces=0;
	switch(base){
		case 'A':
		if(vecino=='G'){
			vecinoA++;
			mutaciones_vecinos(base, vecinoA, i, j);
		}
		break;
		
		case 'C':
		if(vecino=='T'){
			vecinoC++;
			mutaciones_vecinos(base, vecinoC, i, j);
		}
		break;
		
		case 'G':
		if(vecino=='A'){
			vecinoG++;
			mutaciones_vecinos(base, vecinoG, i, j);
		}
		break;
		
		case 'T':
		if(vecino=='C'){
			vecinoT++;
			mutaciones_vecinos(base, vecinoT, i, j);
		}
		break;
	}
	veces++;
	
	if(i==0 || i==dimension){
		if(j==0 || j==dimension){
			if(veces==3){
				vecinoA=0;
				vecinoC=0;
				vecinoG=0;
				vecinoT=0;
				}
			}
		else{
			if(veces==5){
				vecinoA=0;
				vecinoC=0;
				vecinoG=0;
				vecinoT=0;
			}
	}
}
	else if(j==0 || j==dimension){
		if(veces==5){
		vecinoA=0;
		vecinoC=0;
		vecinoG=0;
		vecinoT=0;
		}
	}
	else{
		if(veces==8){
			vecinoA=0;
			vecinoC=0;
			vecinoG=0;
			vecinoT=0;
		}
	}
}


void secuencia(int dimension, int generaciones){
	srand(time(0));
	char adn[dimension][dimension];
	printf("Generacion 0.\n");
		for(int i=0; i<dimension; i++){
			for(int j=0; j<dimension; j++){
				adn[i][j]="ACGT"[rand()%4];
				printf("|%c|", adn[i][j]);
			}
			printf("\n");
		}
		printf("\n");
		
	for(int h=1; h<=generaciones; h++){
		printf("Generación %d.\n", h);
		for(int i=0; i<dimension; i++){
			for(int j=0; j<dimension; j++){
				for(int k=i-1; k<=i+1; k++){
					for(int l=j-1; l<=j+1; l++){
						if((k>0 && k<dimension)&&(l>0 && l<dimension)){
							cuantos_vecinos(adn[i][j], adn[k][l], (dimension-1), i, j);
							}
						}
					}
				}
			}
		printf("\n\n");
	}
	printf("\n");
}

		
int generacion(){
	int generaciones;
	
	printf("Ingrese las generaciones a evaluar: ");
	scanf("%d", &generaciones);
	
	return generaciones;
}

int dimensiones(){
	int dimension;
	
	printf("Ingrese dimension de la matriz: ");
	scanf("%d", &dimension);
	
	return dimension;
}

void menu(int generaciones, int dimension){
	int opcion;
	
	printf("A continuación, ¿qué desea hacer?\n");
	printf("1. Buscar otra secuencia\n2. Cambiar el tamaño de la matriz\n3. Cambiar el número de generaciones\n4. Terminar el juego\n");
	scanf("%d", &opcion);
	
	switch(opcion){
		case 1:
		printf("holi");
		break;
		
		case 2:
		dimension=dimensiones();
		secuencia(dimension, generaciones);
		menu(generaciones, dimension);
		break;
		
		case 3:
		generaciones=generacion();
		secuencia(dimension, generaciones);
		menu(generaciones, dimension);
		break;
		
		case 4:
		printf("¡Gracias por jugar!\n");
		break;
	}
		
}

int main()
{
	int dimension, generaciones;
	
	dimension=dimensiones();
	generaciones=generacion();
	
	secuencia(dimension, generaciones);
	//subsecuencia=subsecuencia();
	
	menu(generaciones, dimension);
	
	return 0;
}

