#include <stdio.h>
#include <stdlib.h>	
#include <time.h>
//adn[i][j], i, j, 'G'

int mutaciones_siguiente(char base, char base_com){
	if(base_com == base){
		return 1;
	}
	else{
		return 0;
	}
}

void matriz_padre(int dimension){
	
	int matriz[dimension][dimension], generaciones;
	char adn[dimension][dimension];
	srand(time(0));
	
	printf("Ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	printf("Generación: \n");
	for(int i=0; i<dimension; i++){
		for(int j=0; j<dimension; j++){
			matriz[i][j]=(rand()%4)+1;
			switch (matriz[i][j]){ 
				case 1:
				adn[i][j]='A';
				printf("|%c|", adn[i][j]);
				break;
				
				case 2:
				adn[i][j]='C';
				printf("|%c|", adn[i][j]);
				break;
				
				case 3:
				adn[i][j]='G';
				printf("|%c|", adn[i][j]);
				break;
				
				case 4:
				adn[i][j]='T';
				printf("|%c|", adn[i][j]);
				break;
				}
			}
			printf("\n");
		}
		
	for(int i=0; i<dimension; i++){
		for(int j=0; j<dimension; j++){
			int mut, mut2=0;
					if(adn[i][j]=='A'){
					for(int k=i-1; k<=i+1; k++){
						for(int l=j-1; l<=j+1; l++){
								mut=mutaciones_siguiente('G', adn[k][l]);
								if(mut==1){
									mut2++;
								}
								mut=0;
							}
						}
						printf("Hay %d G alrededor de A=[%d,%d].\n", mut2, i, j);
					}
					if(adn[i][j]=='C'){
					for(int k=i-1; k<=i+1; k++){
						for(int l=j-1; l<=j+1; l++){
								mut=mutaciones_siguiente('T', adn[k][l]);
								if(mut==1){
									mut2++;
								}
								mut=0;
							}
						}
						printf("Hay %d T alrededor de C=[%d,%d].\n", mut2, i, j);
					}
					if(adn[i][j]=='G'){
					for(int k=i-1; k<=i+1; k++){
						for(int l=j-1; l<=j+1; l++){
								mut=mutaciones_siguiente('A', adn[k][l]);
								if(mut==1){
									mut2++;
								}
								mut=0;
							}
						}
						printf("Hay %d A alrededor de G=[%d,%d].\n", mut2, i, j);
					}
					if(adn[i][j]=='T'){
					for(int k=i-1; k<=i+1; k++){
						for(int l=j-1; l<=j+1; l++){
								mut=mutaciones_siguiente('C', adn[k][l]);
								if(mut==1){
									mut2++;
								}
								mut=0;
							}
						}
						printf("Hay %d C alrededor de T=[%d,%d].\n", mut2, i, j);
					}
			}
			printf("\n");
		}
	}

void tamano_matriz(){
	int dimension;
	printf("Ingrese dimension de la matriz de ADN: ");
	scanf("%d", &dimension);
	
	matriz_padre(dimension);
}
	

int main()
{
	tamano_matriz();
	
	return 0;
}

