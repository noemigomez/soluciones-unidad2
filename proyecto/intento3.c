#include <stdio.h>
#include <stdlib.h>	
#include <time.h>

int mutaciones_siguiente(char base, int base_ev, int k, int l){
	if(l<0 || k<0){
		return 0;
	}
	if(l>5 || k>5){
		return 0;
	}
	else{
		if(base==base_ev){
			return 1;
		}
	}
}

int main()
{
	int matriz[6][6];
	char adn[6][6];
	srand(time(0));

		for(int i=0; i<6; i++){
			for(int j=0; j<6; j++){
				matriz[i][j]=(rand()%4)+1;
				switch (matriz[i][j]){ 
					case 1:
					adn[i][j]='A';
					printf("|%c|", adn[i][j]);
					break;
					case 2:
					adn[i][j]='C';
					printf("|%c|", adn[i][j]);
					break;
					case 3:
					adn[i][j]='G';
					printf("|%c|", adn[i][j]);
					break;
					case 4:
					adn[i][j]='T';
					printf("|%c|", adn[i][j]);
					break;
					}
				}
				printf("\n");
			}
		
		for(int i=0; i<6; i++){
			for(int j=0; j<6; j++){
					int mut=0;
					if(adn[i][j]=='A'){
						int mutA=0;
						for(int k=i-1; k<=i+1; k++){
							for(int l=j-1; l<=j+1; l++){
								mut=0;
								mut=mutaciones_siguiente('G', adn[k][l], k, l);
								if(mut==1){
									mutA++;
								}
							}
						}
						if(mutA==3){
							printf("Hay 3 G, ahora %c (posicion %d, %d) será %c\n", adn[i][j], i, j, 'T');
							adn[i][j]='T';
						}
					}
					if(adn[i][j]=='C'){
						int mutC=0;
						for(int k=i-1; k<=i+1; k++){
							for(int l=j-1; l<=j+1; l++){
								mut=0;
								mut=mutaciones_siguiente('T', adn[k][l], k, l);
								if(mut==1){
									mutC++;
								}
							}
						}
						if(mutC==3){
							printf("Hay 3 T, ahora %c (posicion %d, %d) será %c\n", adn[i][j], i, j, 'G');
							adn[i][j]='G';
						}
					}
					if(adn[i][j]=='G'){
						int mutG=0;
						for(int k=i-1; k<=i+1; k++){
							for(int l=j-1; l<=j+1; l++){
								mut=0;
								mut=mutaciones_siguiente('A', adn[k][l], k, l);
								if(mut==1){
									mutG++;
								}
							}
						}
						if(mutG==3){
							printf("Hay 3 A, ahora %c (posicion %d, %d) será %c\n", adn[i][j], i, j, 'C');
							adn[i][j]='C';
						}
					}
					if(adn[i][j]=='T'){
						int mutT=0;
						for(int k=i-1; k<=i+1; k++){
							for(int l=j-1; l<=j+1; l++){
								mut=0;
								mut=mutaciones_siguiente('C', adn[k][l], k, l);
								if(mut==1){
									mutT++;
								}
							}
						}
						if(mutT==3){
							printf("Hay 3 C, ahora %c (posicion %d, %d) será %c\n", adn[i][j], i, j, 'A');
							adn[i][j]='A';
						}
					}
				}
		}
	return 0;
}

