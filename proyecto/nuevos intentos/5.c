#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//imprime
void imprimir_secuencia(int lado, int hijo, char adn[hijo][lado][lado]){
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			printf("|%c|", adn[hijo][i][j]);
		}
		printf("\n");
	}
}
char cambio_letra(char nucleotido){
	switch(nucleotido){
		case 'A':
		return 'T';
		break;
		case 'C':
		return 'G';
		break;
		case 'G':
		return 'C';
		break;
		case 'T':
		return 'A';
		break;
	}
	return nucleotido;
}
//cambia las letras de acuerdo a las mutaciones
char letra_nueva(int hijo, int lado, char adn[hijo][lado][lado], char frecuencia[lado*lado], int vecinos, int mutacion2, char nucleotido){
	//si tiene 3 correspondientes entre sus vecinos retorna el cambio de letra
	if(vecinos==3){
		return cambio_letra(nucleotido);
	}
	else{
		//si es mayor a 0, la nueva base es la misma anterior
		if(mutacion2>=1){
			return nucleotido;
		}
		else{
			if(hijo>=1){
				return frecuencia[rand()%(lado*lado)];
			}
			else{
				return "ACGT"[rand()%4];
			}
		}
	}
}
//mutaciones de purinas
int mut_fila(int lado, int i, int j, int hijo, char adn[hijo][lado][lado]){
	int nucleotidos, contador;
	if(adn[hijo][i][j]=='G'){
		/*se deben analizar las filas siguientes
		 * por eso k=i+1*/
		int k=i+1;
		//contadores
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[hijo][k][j]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[hijo][i][j]=='A'){
		/*se deben analizar las filas anteriores
		 * por eso k=i-1*/
		int k=i-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[hijo][k][j]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se suma para evaluar el nucleotido siguiente
		}
	}
	return contador;
}
//mutaciones de pirimidinas
int mut_columna(int lado, int i, int j, int hijo, char adn[hijo][lado][lado]){
	int contador, nucleotidos;
	if(adn[hijo][i][j]=='T'){
		/*se deben analizar las columnas siguientes
		 * por eso k=j+1*/
		int k=j+1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[hijo][i][k]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[hijo][i][j]=='C'){
		/*se deben analizar las columnas anteriores
		 * por eso k=j-1*/
		int k=j-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[hijo][i][k]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se suma para evaluar el nucleotido siguiente
		}
	}
	return contador;
}
//calcula lo correspondiente a la segunda mutación
int cuantos_alrededor(int lado, int i, int j, int hijo, char adn[hijo][lado][lado]){
	if(adn[hijo][i][j]=='A' || adn[hijo][i][j]=='G'){
		//retorna la mutacion de la fila debido a que son purinas
		return mut_fila(lado, i, j, hijo, adn);
	}
	else if(adn[hijo][i][j]=='C' || adn[hijo][i][j]=='T'){
		//retorna la mutacion de la columna debido a que son pirimidinas
		return mut_columna(lado, i, j, hijo, adn);
	}
}

//cuenta los vecinos
int cuantos_vecinos(char base, char vecino, int nvecino){
	/*se recibe la cantidad de vecinos que ya se tienen en cuenta
	 * y si el vecino nuevo que se evalúa es el correspondiente se retorna los vecinos recibidos+1*/
	switch(base){
		case 'A':
		if(vecino=='G'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'C':
		if(vecino=='T'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'G':
		if(vecino=='A'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'T':
		if(vecino=='C'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
	}
}

//funcion que calcula cuantas bases (específica) hay dentro de la matriz
int cuantas_hay(int lado, int hijo, char adn[hijo][lado][lado], char buscar){
	int contador=0;
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			if(buscar==adn[hijo][i][j]){
				contador++;
			}
		}
	}
	return contador;
}
//funcion que realiza cambios
void cambio_secuencia(int lado, int hijo, int generacion, char adn[hijo][lado][lado]){
	//variables
	int vecinos[lado][lado], mutacion[lado][lado], vecinijillo;
	//contadores de bases respecto al total
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			/*contadores para evaluar la matriz respecto a la base (vecinos, columnas y filas)
			 * las de dos dimensiones son las que se mantienen con la respectiva letra
			 * mientras que el contador sin dimensiones es temporal*/
			vecinijillo=0;
			vecinos[i][j]=0;
			mutacion[i][j]=0;
			/*recorre los alrededores de la matriz (8 vecinos)*/
			for(int k=i-1; k<=i+1; k++){
				for(int l=j-1; l<=j+1; l++){
					/*en caso de que los valores sean negativos o mayor al largo de la matriz*/
					if((k>=0 && k<lado)&&(l>=0 && l<lado)){
						vecinos[i][j]=cuantos_vecinos(adn[hijo][i][j], adn[hijo][k][l], vecinijillo);
						vecinijillo=vecinos[i][j];
					}
					else{
					vecinijillo=vecinijillo;
					}	
				}
			}
			/*cálculos de frecuencia y la segunda mutacion
			 * cálculo de los alrededores de acuerdo a la segunda funcion*/
			mutacion[i][j]=cuantos_alrededor(lado, i, j, hijo, adn);
		}
	}
	char frecuencia[lado*lado];
	if(hijo>=1){
		int k=0;
		for(int i=0; i<lado; i++){
			for(int j=0; j<lado; j++){
				frecuencia[k]=adn[hijo][i][j];
				k++;
			}
		}
	}
	/*mutaciones*/
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			adn[hijo+1][i][j]=letra_nueva(hijo, lado, adn, frecuencia, vecinos[i][j], mutacion[i][j], adn[hijo][i][j]);
		}
		printf("\n");
	}
	/*mientras la cantidad de hijos realzados sea menor a las generaciones pedidas*/
	if(hijo<generacion){
		//para enviar la recursividad con la siguiente generación
		hijo++;
		printf("Generacion %d: \n", hijo);
		imprimir_secuencia(lado, hijo, adn);
		//se llama la función con la generación mutada
		cambio_secuencia(lado, hijo, generacion, adn);
	}
}
//funcion que realiza la primera secuencia
void secuencia_molde(int lado,int generacion){
	srand(time(0));
	//variable de matriz
	char adn[generacion][lado][lado];
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			//asignacion de bases a aleatoriedad
			adn[0][i][j]="ACGT"[rand()%4];
		}
	}
	//imprimir secuencia molde
	printf("Generación 0: \n");
	imprimir_secuencia(lado, 0, adn);
	
	//realización de cambios
	cambio_secuencia(lado, 0, generacion, adn);
}

//funcion que recibe la dimension
int dimensiones(){
	int dimension;
	
	printf("Por favor, ingrese la dimension de secuencia: ");
	scanf("%d", &dimension);
	
	return dimension;
}
//función que recibe las generaciones
int generacion(){
	int generaciones;
	
	printf("Por favor, ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	return generaciones;
}
char cadena_buscar(int dimension){
	char subsecuencia[dimension];
	printf("Ingrese subsecuencia a buscar: ");
	scanf("%s", &subsecuencia);
	printf("%s", subsecuencia);
	return subsecuencia;
}
void menu(int generaciones, int dimension, char subsecuencia[dimension]){
	int opcion;
	
	printf("A continuación, ¿qué desea hacer?\n");
	printf("1. Buscar otra secuencia\n2. Cambiar el tamaño de la matriz\n3. Cambiar el número de generaciones\n4. Terminar el juego\n");
	scanf("%d", &opcion);
	
	switch(opcion){
		case 1:
		subsecuencia=cadena_buscar(dimension);
		printf("%s", subsecuencia);
		secuencia_molde(dimension, generaciones);
		menu(generaciones, dimension, subsecuencia);
		break;
		
		case 2:
		dimension=dimensiones();
		secuencia_molde(dimension, generaciones);
		menu(generaciones, dimension, subsecuencia);
		break;
		
		case 3:
		generaciones=generacion();
		secuencia_molde(dimension, generaciones);
		menu(generaciones, dimension, subsecuencia);
		break;
		
		case 4:
		printf("¡Gracias por jugar!\n");
		break;
	}
}
void realizar_matrices(){
	//declaracion de variables
	int dimension, generaciones;
	char subsecuencia;
	
	printf("Se verá una secuencia molde y se realizaran mutaciones por generación. \n");
	
	//valorización de variableses mediante función
	dimension=dimensiones();
	generaciones=generacion();
	
	//creacion de secuencias
	secuencia_molde(dimension, generaciones);
	
	//subsecuencia a buscar
	subsecuencia=cadena_buscar(dimension);
	menu(generaciones, dimension, subsecuencia);
}
int main()
{
	//función que realiza el programa
	realizar_matrices();
	
	return 0;
}
