#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void imprimir_secuencia(int lado, int hijo, char adn[hijo][lado][lado]){
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			printf("|%c|", adn[hijo][i][j]);
		}
		printf("\n");
	}
	printf("\n");
}
//~ void secuencia_seguir(int i, int j, int h, int lado, char adn[h][lado][lado], char subsecuencia[lado*lado]){
//~ }
void analizar_i(int i, int j, int h, int lado, char adn[h][lado][lado], char subsecuencia[lado*lado], int largo){
	int contador;
	int bus=0;
	if(j+largo>lado){
		//secuencia_seguir(i, j, h, lado, adn, subsecuencia, largo);
		printf("holi");
	}
	else{
		for(int k=j; k<j+largo; k++){
			if(adn[h][i][k]==subsecuencia[bus]){
				contador++;
				bus++;
			}
		}
		if(contador==largo){
			printf("Se encontró la secuencia en el generación %d. En las coordenadas: ", h); 
			for(int k=j; k<j+largo; k++){
				printf("[%d, %d]; ", i, k);
			}
		}printf("\n");
	}
		
}
void buscar_secuencia(int lado, int hijo, int generacion, char adn[hijo][lado][lado], char subsecuencia[lado*lado]){
	int largo;
	largo=strlen(subsecuencia);
	printf("\n*%s*\n", subsecuencia);
	printf("\n*%d*\n", largo);
	
	for(int h=1; h<=hijo; h++){
		for(int i=0; i<lado; i++){
			for(int j=0; j<lado; j++){
				if(adn[h][i][j]==subsecuencia[0]){
					analizar_i(i, j, h, lado, adn, subsecuencia, largo);
				}
			}
		}
	}
}
char cambio_letra(char nucleotido){
	switch(nucleotido){
		case 'A':
		return 'T';
		break;
		case 'C':
		return 'G';
		break;
		case 'G':
		return 'C';
		break;
		case 'T':
		return 'A';
		break;
	}
	return nucleotido;
}
char letra_nueva(int hijo, int lado, char adn[hijo][lado][lado], char frecuencia[lado*lado], int vecinos, int mutacion2, char nucleotido){
	//si tiene 3 correspondientes entre sus vecinos retorna el cambio de letra
	if(vecinos==3){
		return cambio_letra(nucleotido);
	}
	else{
		//si es mayor a 0, la nueva base es la misma anterior
		if(mutacion2>=1){
			return nucleotido;
		}
		else{
			//si la generacion que se está mutando es la primera (para realizar la segunda)
			if(hijo>=1){
				//se retorna una aleotoriedad respecto a las bases de la secuencia actual (para la secuencia nueva)
				return frecuencia[rand()%(lado*lado)];
			}
			else{
				return "ACGT"[rand()%4];
			}
		}
	}
}
int mut_fila(int lado, int i, int j, int hijo, char adn[hijo][lado][lado]){
	int nucleotidos, contador;
	if(adn[hijo][i][j]=='G'){
		/*se deben analizar las filas siguientes
		 * por eso k=i+1*/
		int k=i+1;
		//contadores
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[hijo][k][j]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[hijo][i][j]=='A'){
		/*se deben analizar las filas anteriores
		 * por eso k=i-1*/
		int k=i-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[hijo][k][j]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se suma para evaluar el nucleotido siguiente
		}
	}
	return contador;
}
int mut_columna(int lado, int i, int j, int hijo, char adn[hijo][lado][lado]){
	int contador, nucleotidos;
	if(adn[hijo][i][j]=='T'){
		/*se deben analizar las columnas siguientes
		 * por eso k=j+1*/
		int k=j+1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[hijo][i][k]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[hijo][i][j]=='C'){
		/*se deben analizar las columnas anteriores
		 * por eso k=j-1*/
		int k=j-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[hijo][i][k]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se suma para evaluar el nucleotido siguiente
		}
	}
	return contador;
}
int cuantos_alrededor(int lado, int i, int j, int hijo, char adn[hijo][lado][lado]){
	if(adn[hijo][i][j]=='A' || adn[hijo][i][j]=='G'){
		//retorna la mutacion de la fila debido a que son purinas
		return mut_fila(lado, i, j, hijo, adn);
	}
	else if(adn[hijo][i][j]=='C' || adn[hijo][i][j]=='T'){
		//retorna la mutacion de la columna debido a que son pirimidinas
		return mut_columna(lado, i, j, hijo, adn);
	}
	return 0;
}
int cuantos_vecinos(char base, char vecino, int nvecino){
	/*se recibe la cantidad de vecinos que ya se tienen en cuenta
	 * y si el vecino nuevo que se evalúa es el correspondiente se retorna los vecinos recibidos+1*/
	switch(base){
		case 'A':
		if(vecino=='G'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'C':
		if(vecino=='T'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'G':
		if(vecino=='A'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'T':
		if(vecino=='C'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
	}
	return nvecino;
}
int cuantas_hay(int lado, int hijo, char adn[hijo][lado][lado], char buscar){
	int contador=0;
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			if(buscar==adn[hijo][i][j]){
				contador++;
			}
		}
	}
	return contador;
}
void mutaciones(int lado, int hijo, int generacion, char adn[hijo][lado][lado], char subsecuencia[lado*lado]){
	printf("\n*%s*\n", subsecuencia);
	//variables
	int vecinos[lado][lado], mutacion[lado][lado], vecinijillo;
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			/*contadores para evaluar la matriz respecto a la base (vecinos, columnas y filas)
			 * las de dos dimensiones son las que se mantienen con la respectiva letra
			 * mientras que el contador sin dimensiones es temporal para evitar errores*/
			vecinijillo=0;
			vecinos[i][j]=0;
			mutacion[i][j]=0;
			/*recorre los alrededores de la matriz (8 vecinos)*/
			for(int k=i-1; k<=i+1; k++){
				for(int l=j-1; l<=j+1; l++){
					/*en caso de que los valores sean negativos o mayor al largo de la matriz*/
					if((k>=0 && k<lado)&&(l>=0 && l<lado)){
						vecinos[i][j]=cuantos_vecinos(adn[hijo][i][j], adn[hijo][k][l], vecinijillo);
						vecinijillo=vecinos[i][j];
					}
					else{
					vecinijillo=vecinijillo;
					}	
				}
			}
			/*cálculos de frecuencia y la segunda mutacion
			 * cálculo de los alrededores de acuerdo a la segunda funcion*/
			mutacion[i][j]=cuantos_alrededor(lado, i, j, hijo, adn);
		}
	}
	//frecuencia de la matriz
	char frecuencia[lado*lado];
	//si la secuencia que está siendo mutadada es la 1, o más, se calcula la frecuencia
	if(hijo>=1){
		int k=0;
		for(int i=0; i<lado; i++){
			for(int j=0; j<lado; j++){
				//arreglo que contiene todas las bases de la secuencia
				frecuencia[k]=adn[hijo][i][j];
				k++;
			}
		}
	}
	/*mutaciones*/
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			adn[hijo+1][i][j]=letra_nueva(hijo, lado, adn, frecuencia, vecinos[i][j], mutacion[i][j], adn[hijo][i][j]);
		}
	}
	/*mientras la cantidad de hijos realzados sea menor a las generaciones pedidas*/
	if(hijo<generacion){
		//para enviar la recursividad con la siguiente generación
		hijo++;
		printf("Generacion %d: \n", hijo);
		imprimir_secuencia(lado, hijo, adn);
		//se llama la función con la generación mutada
		mutaciones(lado, hijo, generacion, adn, subsecuencia);
	}
	else{
		buscar_secuencia(lado, hijo, generacion, adn, subsecuencia);
	}
}
void secuencia_molde(int lado, int generacion, char subsecuencia[lado*lado]){
	printf("\n*%s*\n", subsecuencia);
	srand(time(0));
	//variable de matriz
	char adn[generacion][lado][lado];
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			//asignacion de bases a aleatoriedad
			adn[0][i][j]="ACGT"[rand()%4];
		}
	}
	//imprimir secuencia molde
	printf("Generación 0: \n");
	imprimir_secuencia(lado, 0, adn);
	
	//realización de cambios
	mutaciones(lado, 0, generacion, adn, subsecuencia);
}
int dimensiones(){
	int dimension;
	
	do{
		printf("Por favor, ingrese la dimension de secuencia: ");
		scanf("%d", &dimension);
	}while(dimension<5);
	
	return dimension;
}
//función que recibe las generaciones
int generacion(){
	int generaciones;
	
	printf("Por favor, ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	return generaciones;
}

void realizar_matrices(){
	//declaracion de variables
	int dimension, generaciones;
	
	printf("Se verá una secuencia molde y se realizaran mutaciones por generación. \n");
	
	//valorización de variableses mediante función
	dimension=dimensiones();
	generaciones=generacion();
	
	char subsecuencia[dimension*dimension];
	
	printf("Ingrese subsecuencia a buscar: ");
	scanf(" %[^\n]s", subsecuencia);
	
	printf("\n*%s*\n", subsecuencia);
	
	//creacion de secuencias
	secuencia_molde(dimension, generaciones, subsecuencia);
	
}
int main()
{
	//función que realiza el programa
	realizar_matrices();
	
	return 0;
}
