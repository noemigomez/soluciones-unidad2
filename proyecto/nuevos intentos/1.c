#include <stdio.h>
#include <stdlib.h>
#include <time.h>
void imprimir_secuencia(int lado, char adn[lado][lado]){
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			printf("|%c|", adn[i][j]);
		}
		printf("\n");
	}
}
char cambio_letra(char nucleotido){
	switch(nucleotido){
		case 'A':
		return 'T';
		break;
		case 'C':
		return 'G';
		break;
		case 'G':
		return 'C';
		break;
		case 'T':
		return 'A';
		break;
	}
	return nucleotido;
}
char letra_nueva(int vecinos, int mutacion2, char nucleotido){
	if(vecinos==3){
		return cambio_letra(nucleotido);
	}
	else{
		if(mutacion2>=1){
			return nucleotido;
		}
		else{
			return "ACGT"[rand()%4];
		}
	}
}

int mut_fila(int lado, int i, int j, char adn[lado][lado]){
	int nucleotidos, contador;
	if(adn[i][j]=='G'){
		int k=i+1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[k][j]==adn[i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[i][j]=='A'){
		int k=i-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[k][j]==adn[i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se suma para evaluar el nucleotido siguiente
		}
	}
	return contador;
}
int mut_columna(int lado, int i, int j, char adn[lado][lado]){
	int contador, nucleotidos;
	if(adn[i][j]=='T'){
		int k=j+1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[i][k]==adn[i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[i][j]=='C'){
		int k=j-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[i][k]==adn[i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se suma para evaluar el nucleotido siguiente
		}
	}
	return contador;
}
int cuantos_alrededor(int lado, int i, int j, char adn[lado][lado]){
	if(adn[i][j]=='A' || adn[i][j]=='G'){
		return mut_fila(lado, i, j, adn);
	}
	else if(adn[i][j]=='C' || adn[i][j]=='T'){
		return mut_columna(lado, i, j, adn);
	}
}

int cuantos_vecinos(char base, char vecino, int nvecino){
	switch(base){
		case 'A':
		if(vecino=='G'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'C':
		if(vecino=='T'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'G':
		if(vecino=='A'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'T':
		if(vecino=='C'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
	}
}
void cambio_secuencia(int lado, int hijo, int generacion, char adn[lado][lado]){
	int vecinos[lado][lado], mutacion[lado][lado], vecinijillo;
	char nueva[lado][lado];
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			vecinijillo=0;
			vecinos[i][j]=0;
			mutacion[i][j]=0;
			for(int k=i-1; k<=i+1; k++){
				for(int l=j-1; l<=j+1; l++){
					if((k>=0 && k<lado)&&(l>=0 && l<lado)){
						vecinos[i][j]=cuantos_vecinos(adn[i][j], adn[k][l], vecinijillo);
						vecinijillo=vecinos[i][j];
					}
					else{
					vecinijillo=vecinijillo;
					}	
				}
			}
			mutacion[i][j]=cuantos_alrededor(lado, i, j, adn);
		}
	}
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			printf("%c tiene %d vecinos y %d iguales en su rango; (%d, %d); ", adn[i][j], vecinos[i][j], mutacion[i][j], i, j);
			nueva[i][j]=letra_nueva(vecinos[i][j], mutacion[i][j], adn[i][j]);
			printf("Ahora %c = %c.\n", adn[i][j], nueva[i][j]);
		}
		printf("\n");
	}
	if(hijo<generacion){
		hijo++;
		printf("Generacion %d: \n", hijo);
		imprimir_secuencia(lado, nueva);
		cambio_secuencia(lado, hijo, generacion, nueva);
	}
}
void secuencia_molde(int lado,int generacion){
	srand(time(0));
	char adn[lado][lado];
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			adn[i][j]="ACGT"[rand()%4];
		}
	}
	printf("Generación 0: \n");
	imprimir_secuencia(lado, adn);
	
	cambio_secuencia(lado, 0, generacion, adn);
}

void secuencia(int lado, int generacion){
	//adn molde
	secuencia_molde(lado, generacion);
}

int dimensiones(){
	int dimension;
	
	printf("Por favor, ingrese la dimension de secuencia: ");
	scanf("%d", &dimension);
	
	return dimension;
}

int generacion(){
	int generaciones;
	
	printf("Por favor, ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	return generaciones;
}
void menu(int generaciones, int dimension){
	int opcion;
	
	printf("A continuación, ¿qué desea hacer?\n");
	printf("1. Buscar otra secuencia\n2. Cambiar el tamaño de la matriz\n3. Cambiar el número de generaciones\n4. Terminar el juego\n");
	scanf("%d", &opcion);
	
	switch(opcion){
		case 1:
		printf("holi");
		break;
		
		case 2:
		dimension=dimensiones();
		secuencia(dimension, generaciones);
		menu(generaciones, dimension);
		break;
		
		case 3:
		generaciones=generacion();
		secuencia(dimension, generaciones);
		menu(generaciones, dimension);
		break;
		
		case 4:
		printf("¡Gracias por jugar!\n");
		break;
	}
		
}
void realizar_matrices(){
	int dimension, generaciones;
	
	printf("Se verá una secuencia molde y se realizaran mutaciones por generación. \n");
	
	//valorización de variableses mediante función
	dimension=dimensiones();
	generaciones=generacion();
	
	secuencia(dimension, generaciones);
	menu(generaciones, dimension);
}

int main()
{
	realizar_matrices();
	
	return 0;
}

