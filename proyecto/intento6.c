#include <stdio.h>
#include <stdlib.h>	
#include <time.h>

void imprimir_secuencia(int lado, char adn[lado][lado]){
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			printf("|%c|", adn[i][j]);
		}
		printf("\n");
	}
}
char letra_nueva(char nucleotido){
	switch(nucleotido){
		case 'A':
		return 'T';
		break;
							
		case 'C':
		return 'G';
		break;
							
		case 'G':
		return 'C';
		break;
							
		case 'T':
		return 'A';
		break;
	}
}

int mut_columna(int lado, int i, int j, char adn[lado][lado]){
	int contador=0;
	if(adn[i][j]=='T'){
		for(int k=j+1; k<=j+4; k++){
			if(adn[i][k]==adn[i][j]){
				contador++;
			}
		}
	}
	else if(adn[i][j]=='C'){
		for(int k=j-1; k>=j-4; k--){
			if(adn[i][k]==adn[i][j]){
				contador++;
			}
		}
	}
	return contador;
}

int mut_fila(int lado, int i, int j, char adn[lado][lado]){
	int contador=0;
	if(adn[i][j]=='G'){
		for(int k=i+1; k<=i+4; k++){
			if(adn[k][j]==adn[i][j]){
				contador++;
			}
		}
	}
	else if(adn[i][j]=='A'){
		for(int k=i-1; k>=i-4; k--){
			printf("%c", adn[k][j]);
			if(adn[k][j]==adn[i][j]){
				contador++;
			}
		}
	}
	return contador;
}
//función que realiza las mutaciones
void mutaciones_siguiente(int lado, int vecino[lado][lado], char adn[lado][lado]){
	int mutacion_columna_fila[lado][lado]; //mutacion por cada letra
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			/*la condicion para la segunda mutacion es que los vecinos no sean exactamente 3
			 * por lo que si son 3, se realiza la primera mutacion*/
			if(vecino[i][j]==3){
				adn[i][j]=letra_nueva(adn[i][j]);
				
			}
			else{
				if(adn[i][j]=='T' || adn[i][j]=='C'){
					mutacion_columna_fila[i][j]=mut_columna(lado, i, j, adn);
				}
				else if(adn[i][j]=='A' || adn[i][j]=='G'){
					mutacion_columna_fila[i][j]=mut_fila(lado, i, j, adn);
					}
				
				if(mutacion_columna_fila[i][j]<=0){
					adn[i][j]="ACGT"[rand()%4];
				}
				else{
					adn[i][j]=adn[i][j];
				}
			}
		}
	}
	//se imprime la nueva secuencia
	imprimir_secuencia(lado, adn);
}
//función que verifica los vecinos según con lo que se llama la función
int cuantos_vecinos(char nucleotido, char vecino, char buscado, int nvecino){
	//si el vecino es el que se busca, se retorna la cantidad de vecinos+1
	if(vecino==buscado){
		return nvecino+1;
	}
	else{
		return nvecino;
	}
}

//función que realiza la primera mutación
void mutaciones_uno(int lado, int generaciones, char adn[lado][lado]){
	int vecinos, vecino[lado][lado]; //variables
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			/*se valorizan con 0 debido a que son contadores
			 * hay dos debido a que una es para cada número de la matriz y la otra es general*/
			vecino[i][j]=0; 
			vecinos=0;
			/*se dan nuevos valores en ciclos (k y l) para que recorra los alrededores del número correspondiente*/
			for(int k=i-1; k<=i+1; k++){
				for(int l=j-1; l<=j+1; l++){
					/*condición para valores de dimension no existentes como el -1 o un número mayor al de la dimensión
					 * mediante esta se va llamando a la función que calcula los vecinos
					 * dependiendo de la letra evaluada se envían los valores a la misma función*/
					if((k>=0 && k<lado)&&(l>=0 && l<lado)){
						switch(adn[i][j]){
							case 'A':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'G', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'C':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'T', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'G':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'A', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'T':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'C', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
						}
					}
					else{
						vecino[i][j]=vecinos;
					}
				}
			}
		}
	}
	printf("\n");
	//llamado a función de la segunda mutación y que realiza las mutaciones
	mutaciones_siguiente(lado, vecino, adn);
}

//función que realiza la matriz original 
void secuencia_molde(int lado, int generaciones){
	//semilla de tiempo para la no redudancia de valores aleatorios
	srand(time(0));
	
	char adn[lado][lado]; //matriz
	
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			//se declaran las letras previas para la aleatoriedad
			adn[i][j]="ACGT"[rand()%4];
		}
	}
	
	//se imprime la secuencia
	imprimir_secuencia(lado, adn);
	//for(int h=1; h<=generaciones; h++){
						//se realizan las mutaciones para generaciones
	/*adn[lado][lado]=*/mutaciones_uno(lado, generaciones, adn);
	//imptimir_secuencia(lado, adn);
	//}

}

//función que solicita las generaciones
int generacion(){
	int generaciones;
	
	printf("Por favor, ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	return generaciones;
}

//función que solicita la dimension
int dimension(){
	int dimensiones;
	
	printf("Por favor, ingrese la dimension de secuencia: ");
	scanf("%d", &dimensiones);
	
	return dimensiones;
}


int main()
{
	int dimensiones, generaciones;
	
	//valorización de variableses mediante función
	dimensiones=dimension();
	generaciones=generacion();
	
	//llamado a la función para la secuencia origial
	secuencia_molde(dimensiones, generaciones);
	
	//~ char primera_secuencia[dimensiones][dimensiones];
	
	//~ primera_secuencia[dimensiones][dimensiones]=secuencia_molde(dimensiones);
	//~ mutaciones_uno(dimensiones, generaciones, primera_secuencia);
	//subsecuencia=subsecuencia();
	
	//menu(generaciones, dimensiones);
	
	return 0;
}

