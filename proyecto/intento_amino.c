#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
void imprimir_aminoacidos(int aminoacidos, char aa[aminoacidos]){
	for(int i=0; i<aminoacidos; i++){
		printf("|%c|", aa[i]);
	}
}
char aminoacido_a(int aminoacidos, int i, char amino[i][3]){
	if(amino[i][1]=='A'){
		if(amino[i][2]=='A' || amino[i][2]=='G'){
			return 'K';
		}
		else{
			return 'N';
		}
	}
	else if(amino[i][1]=='C'){
		return 'T';
	}
	else if(amino[i][1]=='G'){
		if(amino[i][2]=='A' || amino[i][2]=='G'){
			return 'R';
		}
		else{
			return 'S';
		}
	}
	else if(amino[i][1]=='T'){
		if(amino[i][2]=='G'){
			return 'M';
		}
		else{
			return 'I';
		}
	}
}
char aminoacido_c(int aminoacidos, int i, char amino[i][3]){
	if(amino[i][1]=='A'){
		if(amino[i][2]=='A' || amino[i][2]=='G'){
			return 'Q';
		}
		else{
			return 'H';
		}
	}
	else if(amino[i][1]=='C'){
		return 'P';
	}
	else if(amino[i][1]=='G'){
		return 'R';
	}
	else if(amino[i][1]=='T'){
		return 'L';
	}
}
char aminoacido_g(int aminoacidos, int i, char amino[i][3]){
	if(amino[i][1]=='A'){
		if(amino[i][2]=='A' || amino[i][2]=='G'){
			return 'E';
		}
		else{
			return 'D';
		}
	}
	else if(amino[i][1]=='C'){
		return 'A';
	}
	else if(amino[i][1]=='G'){
		return 'G';
	}
	else if(amino[i][1]=='T'){
		return 'V';
	}
}
char aminoacido_t(int aminoacidos, int i, char amino[i][3]){
	if(amino[i][1]=='A'){
		if(amino[i][2]=='A' || amino[i][2]=='G'){
			return '-';
		}
		else{
			return 'Y';
		}
	}
	else if(amino[i][1]=='C'){
		return 'S';
	}
	else if(amino[i][1]=='G'){
		if(amino[i][2]=='T' || amino[i][2]=='C'){
			return 'C';
		}
		else if(amino[i][2]=='A'){
			return '-';
		}
		else{
			return 'W';
		}
	}
	else if(amino[i][1]=='T'){
		if(amino[i][2]=='A' || amino[i][2]=='G'){
			return 'L';
		}
		else{
			return 'F';
		}
	}
}
void que_aminoacidos(int aminoacidos, char amino[aminoacidos][3]){
	char aa[aminoacidos];
	for(int i=0; i<aminoacidos; i++){
		if(amino[i][0]=='A'){
			aa[i]=aminoacido_a(aminoacidos, i, amino);
		}
		else if(amino[i][0]=='C'){
			aa[i]=aminoacido_c(aminoacidos, i, amino);
		}
		else if(amino[i][0]=='G'){
			aa[i]=aminoacido_g(aminoacidos, i, amino);
		}
		else if(amino[i][0]=='T'){
			aa[i]=aminoacido_t(aminoacidos, i, amino);
		}
	}
	imprimir_aminoacidos(aminoacidos, aa);
}

void transformar(char secuencia[10000]){
	int aminoacidos, resto, largo;
	largo=strlen(secuencia);
	resto=largo%3;
	aminoacidos=(largo-resto)/3;
	
	char amino[aminoacidos][3];
	int k=0;
	
	for(int i=0; i<aminoacidos; i++){
		for(int j=0; j<3; j++){
			amino[i][j]=secuencia[k];
			k++;
		}
	}
	que_aminoacidos(aminoacidos, amino);
	printf("\nHay %d nucleotidos sobrantes que no completan un codón.\n", resto);
}

int main()
{
	char secuencia[100000];
	printf("Ingrese secuencia: ");
	scanf(" %[^\n]s", secuencia);
	
	transformar(secuencia);
	
	return 0;
}

