#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//imprime matrices
void imprimir(int matriz[5][5], int numero){
	printf("\nMatriz %d:\n", numero);
	for(int i=0; i<5; i++){
		for(int j=0; j<5; j++){
			printf(" |%d|", matriz[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}
//realiza multiplicacion
int multiplicacion(int x, int y, int matriz_1[5][5], int matriz_2[5][5]){
	//variables
	int valor[5], retornar=0;
	for(int i=0; i<5; i++){
		//se multiplica término a término columnas y filas correspondientes
		valor[i]=matriz_1[x][i]*matriz_2[i][y];
	}
	//se suman los valores para la multiplicacion a retornar
	for(int i=0; i<5; i++){
		retornar=retornar+valor[i];
	}
	return retornar;
}

//multiplica las matrices
void multiplicar_matrices(int matriz_1[5][5], int matriz_2[5][5]){
	//matriz multiplicada
	int matriz_3[5][5];
	
	for(int x=0; x<5; x++){
		for(int y=0; y<5; y++){
			matriz_3[x][y]=multiplicacion(x, y, matriz_1, matriz_2);
		}
	}
	imprimir(matriz_3, 3);
}

int main()
{
	//variables y semilla de tiempo
	int aleatorio, matriz_1[5][5], matriz_2[5][5];
	srand(time(0));
	
	//se repite si el número ingresado no es positivo
	do{
		printf("Ingrese límite para número de matriz: ");
		scanf("%d", &aleatorio);
	}while(aleatorio<1);
	
	//se generan ambas matrices producto
	for(int i=0; i<5; i++){
		for(int j=0; j<5; j++){
			matriz_1[i][j]=rand()%aleatorio;
			matriz_2[i][j]=rand()%aleatorio;
		}
	}
	//se imprimen las matrices
	imprimir(matriz_1, 1);
	imprimir(matriz_2, 2);
	
	multiplicar_matrices(matriz_1, matriz_2);
	
	return 0;
}

