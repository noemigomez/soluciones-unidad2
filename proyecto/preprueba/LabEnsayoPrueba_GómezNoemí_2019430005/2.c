#include <stdio.h>
#include <stdlib.h>

//imprime
void imprimir(int arreglo[15]){
	for(int i=0; i<15; i++){
		if(i!=14){
			printf("%d, ", arreglo[i]);
		}
		else{
			//si i es el último término, no imprimir con él una coma
			printf("%d\n", arreglo[i]);
		}
	}
}
//ordena el arreglo
void ordenar(int arreglo_inicial[15]){
	//variables
	int mayor, indice, arreglo_final[15];
	for(int j=0; j<15; j++){
		//se declara el mayor como 0, variable que se reemplaza a medida que se encuentra uno mayor al anterior
		mayor=0;
		for(int i=0; i<15; i++){
			if(arreglo_inicial[i]>mayor){
				mayor=arreglo_inicial[i];
				//variable que guarda el índice del mayor
				indice=i;
			}
		}
		/*se iguala el arreglo 0 como el mayor y el equivalente a este del arreglo inicial se iguala a 0
		 * esto es para luego no confundir el mayor, y se evalúe el siguiente mayor */
		arreglo_final[j]=mayor;
		arreglo_inicial[indice]=0;
	}
	imprimir(arreglo_final);
}

int main()
{
	//arreglo
	int arreglo_inicial[15]={ 8, 43, 17, 88, 1, 5, 11, 34, 56, 77, 99, 3, 7, 25, 30 };
	//funciones que imprimen y ordenan el arreglo
	imprimir(arreglo_inicial);
	ordenar(arreglo_inicial);
	
	return 0;
}

