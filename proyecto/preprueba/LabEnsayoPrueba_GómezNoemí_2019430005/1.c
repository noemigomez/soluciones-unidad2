#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	//variables
	int largo;
	char frase[1000];
	printf("Ingrese cadena: ");
	gets(frase);//recibo de variable
	
	//strlen para ver el largo de lo recibido
	largo=strlen(frase);
	
	//se imprime la frase normal
	printf("Normal: ");
	for(int i=0; i<largo; i++){
		printf("%c", frase[i]);
	}
	printf("\n");
	//si imprime la frase cifrada
	printf("Cifrado: ");
	for(int i=0; i<largo; i++){
		/*los caracteres de la a a la z, minúscula, tienen los números del 97 al 122 en ascii
		 * se señala el alfabeto normal es en minúscula*/
		if(frase[i]>=97 && frase[i]<=122){
			/*en el caso de la x, y, z se debe volver a comenzar con el abecedario*/
			if(frase[i]=='x' || frase[i]=='y' || frase[i]=='z'){
				printf("%c", frase[i]-55);
			}
			else{
				printf("%c", frase[i]-29);
			}
		}
		else{
		printf("%c", frase[i]);
		}
	}
}

