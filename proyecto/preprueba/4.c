#include <stdio.h>
#include <stdlib.h>
#include <time.h>
//imprime
void imprimir(int N, int matriz[N][N]){
	for(int i=0; i<N; i++){
		for(int j=0; j<N; j++){
			printf("[%d]", matriz[i][j]);
		}
		printf("\n");
	}
}

//revisa si saltó por toda la matriz
int salto_todo_ya(int N, int matriz[N][N]){
	//variable, contador
	int todo=0;
	for(int i=0; i<N; i++){
		for(int j=0; j<N; j++){
			//si hay un cero, significa que no ha pasado por ahí
			if(matriz[i][j]==0){
				todo++;
			}
		}
	}
	return todo;
}

//se genera el nuevo salto
int nuevo_salto(int x, int resto){
	/*variables
	 * i, cuanto se mueve
	 * evalua, hacia donde se mueve*/
	int i, evalua;
	i=((rand()%3)+1);
	evalua=rand()%2;
	if(resto<0){
		//si se mueve más de 3 veces, retorna 0 posiciones
		if(resto-i<-3){
			return 0;
		}
	}
	else{
		//si se mueve más de 3 veces, retorna 0 posiciones
		if(resto+i>3){
			return 0;
		}
	}
	
	if(evalua==0){
		i=i*(-1);
	}
	return i;
}

//salta la rana
void saltos_rana(int N, int matriz[N][N], int x, int y){
	/*variables 
	 * i y j, coordenadas nuevas del salto
	 * ya paso cuenta si donde pasa ya pasó antes
	 * no hay cuenta si quiere saltar donde no existe*/
	int saltos=0, i, j, todo, ya_paso=0;
	int no_hay=0;
	//salta hasta que pase por todas las casillas
	while(todo>0){
		i=nuevo_salto(x, 0);
		j=nuevo_salto(y, i);
		//si el salto no está dentro de los límites
		if(i+x<0 || j+y<0 || i+x>=N || j+y>=N){
			no_hay++;
		}
		else{
			//se suma el salto
			matriz[x+i][y+j]=matriz[x+i][y+j]+1;
			if(matriz[x+i][y+j]>1){
				ya_paso++;
			}
			saltos++;
			printf("\nSalto %d: \n", saltos);
			imprimir(N, matriz);
			/*se actualiza la posicion de la rana*/
			x=x+i;
			y=y+j;
		}
		//se evalúa la matriz, si ya saltó por todas las casillas
		todo=salto_todo_ya(N, matriz);
	}
	
	//se imprimen datos finales 
	printf("\nLa ranita dió %d saltos.", saltos);
	printf("\nLa ranita pasó %d por donde ya había pasado.", ya_paso);
	printf("\nTrató %d veces de saltar en una casilla que no existe.", no_hay);
	
}

int main(){
	//semilla de tiempo
	srand(time(0));
	/*variables
	 * N, dimension de matrices
	 * x, coordenada de columna
	 * y, coordenada de fila*/
	int N, x, y;
	
	printf("Una rana saltará por una matriz de dimensiones a solicitar.\n");
	printf("Se imprimirá en cada casilla el número de veces que la rana ha pasado por ahí.\n");
	do{
		printf("Ingrese dimension de tablero donde saltará la ranita: ");
		scanf("%d", &N);
	}while(N<1);//tiene que ser entero positivo
	
	//matriz
	int matriz[N][N];
	
	//llenado de posiciones 0 inicial
	for(int i=0; i<N; i++){
		for(int j=0; j<N; j++){
			matriz[i][j]=0;
		}
	}
	//imprimir
	printf("Tablero sin rana: \n");
	imprimir(N, matriz);


	printf("Ingrese posiciones de la rana [x, y].\n Posiciones válidas entre 0 y %d.\n", N-1);
	do{
		printf("x: ");
		scanf("%d", &x);
	}while(x<0 || x>=N); //posiciones menores a 0 o mayores al largo de la matriz no son válidas
	do{
		printf("y: ");
		scanf("%d", &y);
	}while(y<0 || y>=N); //posiciones menores a 0 o mayores al largo de la matriz no son válidas
	
	//se establece la posición inicial
	matriz[x][y]=1;
	printf("Posición inicial rana: \n");
	imprimir(N, matriz);
	
	//funcion que realiza los saltos
	saltos_rana(N, matriz, x, y);
	
	return 0;
}

