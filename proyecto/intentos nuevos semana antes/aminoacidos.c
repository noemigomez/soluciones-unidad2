#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
void imprimir_aminoacidos(int aminoacidos, char aa[aminoacidos]){
	for(int i=0; i<aminoacidos; i++){
		printf("|%c|", aa[i]);
	}
}
char aminoacidos_c(int i, char amino[i][3]){
	char combinaciones[16][3]={ "CAA", "CAC", "CAG", "CAT", "CCA", "CCC", "CCG", "CCT", "CGA", "CGC", "CGG", "CGT", "CTA", "CTC", "CTG", "CTT" };
	char aminoacidos[16]={ "QHQHPPPPRRRRLLLL" };
	int contador=0, encontrado;
	for(int j=0; j<16; j++){
		for(int k=0; k<3; k++){
			if(combinaciones[j][k]==amino[i][k]){
				contador++;
			}
			else{
				contador=0;
			}
		}
		if(contador==3){
			encontrado=j;
		}
	}
	return aminoacidos[encontrado];
}
char aminoacidos_g(int i, char amino[i][3]){
	char combinaciones[16][3]={ "GAA", "GAC", "GAG", "GAT", "GCA", "GCC", "GCG", "GCT", "GGA", "GGC", "GGG", "GGT", "GTA", "GTC", "GTG", "GTT" };
	char aminoacidos[16]={ "EDEDAAAAGGGGVVVV" };
	int contador=0, encontrado;
	for(int j=0; j<16; j++){
		for(int k=0; k<3; k++){
			if(combinaciones[j][k]==amino[i][k]){
				contador++;
			}
			else{
				contador=0;
			}
		}
		if(contador==3){
			encontrado=j;
		}
	}
	return aminoacidos[encontrado];
}
char aminoacidos_t(int i, char amino[i][3]){
	char combinaciones[16][3]={ "TAA", "TAC", "TAG", "TAT", "TCA", "TCC", "TCG", "TCT", "TGA", "TGC", "TGG", "TGT", "TTA", "TTC", "TTG", "TTT" };
	char aminoacidos[16]={ "-Y-YSSSS-CWCLFLF" };
	int contador=0, encontrado;
	for(int j=0; j<16; j++){
		for(int k=0; k<3; k++){
			if(combinaciones[j][k]==amino[i][k]){
				contador++;
			}
			else{
				contador=0;
			}
		}
		if(contador==3){
			encontrado=j;
		}
	}
	return aminoacidos[encontrado];
}
char aminoacidos_a(int i, char amino[i][3]){
	char combinaciones[16][3]={ "AAA", "AAC", "AAG", "AAT", "ACA", "ACC", "ACG", "ACT", "AGA", "AGC", "AGG", "AGT", "ATA", "ATC", "ATG", "ATT" };
	char aminoacidos[16]={ "KNKNTTTTRSRSIIMI" };
	int contador=0, encontrado;
	for(int j=0; j<16; j++){
		for(int k=0; k<3; k++){
			if(combinaciones[j][k]==amino[i][k]){
				contador++;
			}
			else{
				contador=0;
			}
		}
		if(contador==3){
			encontrado=j;
		}
	}
	return aminoacidos[encontrado];
}
void transformar_aa(int aminoacidos, char amino[aminoacidos][3]){
	char aa[aminoacidos];
	for(int i=0; i<aminoacidos; i++){
		switch(amino[i][0]){
			case 'A': aa[i]=aminoacidos_a(i, amino); break;
			case 'C': aa[i]=aminoacidos_c(i, amino); break;
			case 'G': aa[i]=aminoacidos_g(i, amino); break;
			case 'T': aa[i]=aminoacidos_t(i, amino); break;		 
		}
	}
	imprimir_aminoacidos(aminoacidos, aa);
}
//divide la secuencia en codones
void transformar(char secuencia[10000]){
	int aminoacidos, resto, largo;
	largo=strlen(secuencia);
	resto=largo%3;
	aminoacidos=(largo-resto)/3;
	
	char amino[aminoacidos][3];
	int k=0;
	
	for(int i=0; i<aminoacidos; i++){
		for(int j=0; j<3; j++){
			amino[i][j]=secuencia[k];
			k++;
		}
	}
	transformar_aa(aminoacidos, amino);
	printf("\nHay %d nucleotidos sobrantes que no completan un codón.\n", resto);
}

int main()
{
	char secuencia[100000];
	printf("Ingrese secuencia: ");
	scanf(" %[^\n]s", secuencia);
	
	transformar(secuencia);
	
	return 0;
}
