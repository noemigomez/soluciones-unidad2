#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
//imprime
void imprimir_secuencia(int lado, int hijo, char adn[hijo][lado][lado]){
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			printf("|%c|", adn[hijo][i][j]);
		}
		printf("\n");
	}
	printf("\n");
}
void imprimir_aminoacidos(int aminoacidos, char aa[aminoacidos]){
	printf("\nLos aminoácidos codificantes por la secuencia ingresada son: ");
	for(int i=0; i<aminoacidos; i++){
		printf("|%c|", aa[i]);
	}
}
void imprimir_encuentros(int veces, int encontrado_i[veces], int encontrado_j[veces]){
	printf("\nEncontrada en: ");
	for(int k=0; k<veces; k++){
		printf("[%d, %d]", encontrado_i[k], encontrado_j[k]);
	}
}
void transformar_aa(int aminoacidos, char amino[aminoacidos][3]){
	char aa[aminoacidos];
	char combinaciones[64][3]={ "AAA", "AAC", "AAG", "AAT", "ACA", "ACC", "ACG", "ACT", "AGA", "AGC", "AGG", "AGT", "ATA", "ATC", "ATG", "ATT", "CAA", "CAC", "CAG", "CAT", "CCA", "CCC", "CCG", "CCT", "CGA", "CGC", "CGG", "CGT", "CTA", "CTC", "CTG", "CTT", "GAA", "GAC", "GAG", "GAT", "GCA", "GCC", "GCG", "GCT", "GGA", "GGC", "GGG", "GGT", "GTA", "GTC", "GTG", "GTT", "TAA", "TAC", "TAG", "TAT", "TCA", "TCC", "TCG", "TCT", "TGA", "TGC", "TGG", "TGT", "TTA", "TTC", "TTG", "TTT" };
	char aminoacido[64]={ "KNKNTTTTRSRSIIMIQHQHPPPPRRRRLLLLEDEDAAAAGGGGVVVV-Y-YSSSS-CWCLFLF" };
	int contador=0, encontrado;
	for(int i=0; i<aminoacidos; i++){
		for(int j=0; j<64; j++){
			for(int k=0; k<3; k++){
				if(combinaciones[j][k]==amino[i][k]){
					contador++;
				}
				else{
					contador=0;
				}
			}
			if(contador==3){
				encontrado=j;
			}
		}
		aa[i]=aminoacido[encontrado];
		contador=0;
	}
	imprimir_aminoacidos(aminoacidos, aa);
}
void cambiar_aminoacidos(int largo, char secuencia[largo]){
	int aminoacidos, resto;
	resto=largo%3;
	aminoacidos=(largo-resto)/3;
	
	char amino[aminoacidos][3];
	int k=0;
	
	for(int i=0; i<aminoacidos; i++){
		for(int j=0; j<3; j++){
			amino[i][j]=secuencia[k];
			k++;
		}
	}
	transformar_aa(aminoacidos, amino);
	printf("\nHay %d nucleotidos sobrantes que no completan un codón.\n", resto);
}
void segundadiagonal(int lado, int hijo, char adn[hijo][lado][lado], int i, int j, int largo, char secuencia[largo]){
	int contador=0, encontrado_i[largo], encontrado_j[largo];
	for(int k=i; k<i+largo; k++){
		if(adn[hijo][k][lado-(k+1)]==secuencia[contador]){
			encontrado_i[contador]=k;
			encontrado_j[contador]=lado-(k+1);
			contador++;
		}
	}
	if(contador==largo){
		imprimir_encuentros(contador, encontrado_i, encontrado_j);
	}
}
void diagonal(int lado, int hijo, char adn[hijo][lado][lado], int i, int j, int largo, char secuencia[largo]){
	int contador=0, encontrado_i[largo], encontrado_j[largo];
	for(int k=i; k<i+largo; k++){
		if(adn[hijo][k][k]==secuencia[contador]){
			encontrado_i[contador]=k;
			encontrado_j[contador]=k;
			contador++;
		}
	}
	if(contador==largo){
		imprimir_encuentros(contador, encontrado_i, encontrado_j);
	}
}
void vertical(int lado, int hijo, char adn[hijo][lado][lado], int i, int j, int largo, char secuencia[largo]){
	int contador=0;
	int encontrada_i[largo], encontrada_j[largo];
	if(i+largo<=lado){
		for(int k=i; k<i+largo; k++){
			if(secuencia[contador]==adn[hijo][k][j]){
				encontrada_i[contador]=k;
				encontrada_j[contador]=j;
				contador++;
			}
		}
		if(contador==largo){
			imprimir_encuentros(contador, encontrada_i, encontrada_j);
			contador=0;
		}
	}
}
void horizontal(int lado, int hijo, char adn[hijo][lado][lado], int i, int j, int largo, char secuencia[largo]){
	/*contador que cuenta cuantas veces se realiza el ciclo
	 * veces cuenta las veces que se encuentra la coincidencia*/
	int contador, veces;
	int encontrado_i[largo], encontrado_j[largo]; //coordenadas para imprimir
	contador=0;
	veces=0;
	while(contador<largo){
		//si j es mayor al largo de la matriz, se pasa a la fila siguiente
		if(j>=lado){
			j=j-lado;
			i++;
		}
		//si se encuentra, se guardan las variables y aumenta j para evaluar la siguiente
		if(secuencia[contador]==adn[hijo][i][j]){
			encontrado_i[veces]=i;
			encontrado_j[veces]=j;
			veces++;
			j++;
		}
		else{
			veces=0;
		}
		contador++;
	}
	//si se encuentra tantas veces como el largo, se encontró la secuencia
	if(veces==largo){
		imprimir_encuentros(veces, encontrado_i, encontrado_j);
	}
}
void encontrar_secuencia(int lado, int hijo, char adn[hijo][lado][lado], int largo, char secuencia[largo], int buscar){
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			if(secuencia[0]==adn[hijo][i][j]){
				horizontal(lado, hijo, adn, i, j, largo, secuencia);
				vertical(lado, hijo, adn, i, j, largo, secuencia);
				if(i==j){
					diagonal(lado, hijo, adn, i, j, largo, secuencia);
				}
				else if(i+j==(lado-1)){
					segundadiagonal(lado, hijo, adn, i, j, largo, secuencia);
				}
			}
		}
	}
	if(buscar==0){
		char sec_nueva[largo];
		int b=largo-1;
		for(int a=0; a<largo; a++){
			sec_nueva[a]=secuencia[b];
			b--;
		}
		printf("\nREALIZANDO BÚSQUEDA EN REVERSA\n");
		encontrar_secuencia(lado, hijo, adn, largo, sec_nueva, 1);
	}
}

void buscar_secuencia(int lado, int hijo, int generacion, char adn[hijo][lado][lado], int largo, char secuencia[largo]){
	encontrar_secuencia(lado, 1, adn, largo, secuencia, 0);
}
char cambio_letra(char nucleotido){
	switch(nucleotido){
		case 'A': return 'T'; break;
		case 'C': return 'G'; break;
		case 'G': return 'C'; break;
		case 'T': return 'A'; break;
	}
	return nucleotido;
}
//cambia las letras de acuerdo a las mutaciones
char letra_nueva(int hijo, int lado, char adn[hijo][lado][lado], char frecuencia[lado*lado], int vecinos, int mutacion2, char nucleotido){
	//si tiene 3 correspondientes entre sus vecinos retorna el cambio de letra
	if(vecinos==3){
		return cambio_letra(nucleotido);
	}
	else{
		//si es mayor a 0, la nueva base es la misma anterior
		if(mutacion2>=1){
			return nucleotido;
		}
		else{
			//si la generacion que se está mutando es la primera (para realizar la segunda)
			if(hijo>=1){
				//se retorna una aleotoriedad respecto a las bases de la secuencia actual (para la secuencia nueva)
				return frecuencia[rand()%(lado*lado)];
			}
			else{
				return "ACGT"[rand()%4];
			}
		}
	}
}
//mutaciones de purinas
int mut_fila(int lado, int i, int j, int hijo, char adn[hijo][lado][lado]){
	int nucleotidos, contador;
	if(adn[hijo][i][j]=='G'){
		/*se deben analizar las filas siguientes
		 * por eso k=i+1*/
		int k=i+1;
		//contadores
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[hijo][k][j]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[hijo][i][j]=='A'){
		/*se deben analizar las filas anteriores
		 * por eso k=i-1*/
		int k=i-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[hijo][k][j]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se suma para evaluar el nucleotido siguiente
		}
	}
	return contador;
}
//mutaciones de pirimidinas
int mut_columna(int lado, int i, int j, int hijo, char adn[hijo][lado][lado]){
	int contador, nucleotidos;
	if(adn[hijo][i][j]=='T'){
		/*se deben analizar las columnas siguientes
		 * por eso k=j+1*/
		int k=j+1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[hijo][i][k]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[hijo][i][j]=='C'){
		/*se deben analizar las columnas anteriores
		 * por eso k=j-1*/
		int k=j-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[hijo][i][k]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se suma para evaluar el nucleotido siguiente
		}
	}
	return contador;
}
//calcula lo correspondiente a la segunda mutación
int cuantos_alrededor(int lado, int i, int j, int hijo, char adn[hijo][lado][lado]){
	if(adn[hijo][i][j]=='A' || adn[hijo][i][j]=='G'){
		//retorna la mutacion de la fila debido a que son purinas
		return mut_fila(lado, i, j, hijo, adn);
	}
	else if(adn[hijo][i][j]=='C' || adn[hijo][i][j]=='T'){
		//retorna la mutacion de la columna debido a que son pirimidinas
		return mut_columna(lado, i, j, hijo, adn);
	}
	return 0;
}

//cuenta los vecinos
int cuantos_vecinos(char base, char vecino, int nvecino){
	/*se recibe la cantidad de vecinos que ya se tienen en cuenta
	 * y si el vecino nuevo que se evalúa es el correspondiente se retorna los vecinos recibidos+1*/
	switch(base){
		case 'A':
		if(vecino=='G'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'C':
		if(vecino=='T'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'G':
		if(vecino=='A'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'T':
		if(vecino=='C'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
	}
	return nvecino;
}
//funcion que realiza cambios
void mutaciones(int lado, int hijo, int generacion, char adn[hijo][lado][lado], int largo, char secuencia[largo]){
	//variables
	int vecinos[lado][lado], mutacion[lado][lado], vecinijillo;
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			/*contadores para evaluar la matriz respecto a la base (vecinos, columnas y filas)
			 * las de dos dimensiones son las que se mantienen con la respectiva letra
			 * mientras que el contador sin dimensiones es temporal para evitar errores*/
			vecinijillo=0;
			vecinos[i][j]=0;
			mutacion[i][j]=0;
			/*recorre los alrededores de la matriz (8 vecinos)*/
			for(int k=i-1; k<=i+1; k++){
				for(int l=j-1; l<=j+1; l++){
					/*en caso de que los valores sean negativos o mayor al largo de la matriz*/
					if((k>=0 && k<lado)&&(l>=0 && l<lado)){
						vecinos[i][j]=cuantos_vecinos(adn[hijo][i][j], adn[hijo][k][l], vecinijillo);
						vecinijillo=vecinos[i][j];
					}
					else{
					vecinijillo=vecinijillo;
					}	
				}
			}
			/*cálculos de frecuencia y la segunda mutacion
			 * cálculo de los alrededores de acuerdo a la segunda funcion*/
			mutacion[i][j]=cuantos_alrededor(lado, i, j, hijo, adn);
		}
	}
	//frecuencia de la matriz
	char frecuencia[lado*lado];
	//si la secuencia que está siendo mutadada es la 1, o más, se calcula la frecuencia
	if(hijo>=1){
		int k=0;
		for(int i=0; i<lado; i++){
			for(int j=0; j<lado; j++){
				//arreglo que contiene todas las bases de la secuencia
				frecuencia[k]=adn[hijo][i][j];
				k++;
			}
		}
	}
	/*mutaciones*/
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			adn[hijo+1][i][j]=letra_nueva(hijo, lado, adn, frecuencia, vecinos[i][j], mutacion[i][j], adn[hijo][i][j]);
		}
	}
	/*mientras la cantidad de hijos realzados sea menor a las generaciones pedidas*/
	if(hijo<generacion){
		//para enviar la recursividad con la siguiente generación
		hijo++;
		printf("Generacion %d: \n", hijo);
		imprimir_secuencia(lado, hijo, adn);
		//se llama la función con la generación mutada
		mutaciones(lado, hijo, generacion, adn, largo, secuencia);
	}
	else{
		buscar_secuencia(lado, hijo, generacion, adn, largo, secuencia);
		cambiar_aminoacidos(largo, secuencia);
	}
}
//funcion que realiza la primera secuencia
void secuencia_molde(int lado, int generacion){
	srand(time(0));
	//variable de matriz
	char adn[generacion][lado][lado];
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			//asignacion de bases a aleatoriedad
			adn[0][i][j]="ACGT"[rand()%4];
		}
	}
	char secuencia[lado*lado];
	printf("Ingrese secuencia: ");
	scanf("%s", secuencia);
	int largo;
	largo=strlen(secuencia);
	printf("-%d-", largo);
	
	//imprimir secuencia molde
	printf("Generación 0: \n");
	imprimir_secuencia(lado, 0, adn);
	
	//realización de cambios
	mutaciones(lado, 0, generacion, adn, largo, secuencia);
}

//funcion que recibe la dimension
int dimensiones(){
	int dimension;
	
	printf("Por favor, ingrese la dimension de secuencia: ");
	scanf("%d", &dimension);
	
	return dimension;
}
//función que recibe las generaciones
int generacion(){
	int generaciones;
	
	printf("Por favor, ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	return generaciones;
}
//funcion que realiza el programa
void realizar_matrices(){
	//declaracion de variables
	int dimension, generaciones;
	
	printf("Se verá una secuencia molde y se realizaran mutaciones por generación. \n");
	
	//valorización de variableses mediante función
	dimension=dimensiones();
	generaciones=generacion();
	
	//creacion de secuencias
	secuencia_molde(dimension, generaciones);
}
int main()
{
	//función que realiza el programa
	realizar_matrices();
	
	return 0;
}
