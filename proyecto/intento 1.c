#include <stdio.h>
#include <stdlib.h>	
#include <time.h>

void matriz_ADN(int dimension){
	
	int matriz[dimension][dimension], generaciones;
	char adn[dimension][dimension];
	srand(time(0));
	
	printf("Ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	for(int h=1; h<=generaciones; h++){
		printf("Generación %d: \n", h);
		for(int i=0; i<dimension; i++){
			for(int j=0; j<dimension; j++){
				matriz[i][j]=(rand()%4)+1;
				switch (matriz[i][j]){ 
					case 1:
					adn[i][j]='A';
					printf("|%c|", adn[i][j]);
					break;
					case 2:
					adn[i][j]='C';
					printf("|%c|", adn[i][j]);
					break;
					case 3:
					adn[i][j]='G';
					printf("|%c|", adn[i][j]);
					break;
					case 4:
					adn[i][j]='T';
					printf("|%c|", adn[i][j]);
					break;
					}
				}
				printf("\n");
			}
		printf("\n");
	}
}

void tamano_matriz(){
	int dimension;
	printf("Ingrese dimension de la matriz de ADN: ");
	scanf("%d", &dimension);
	
	matriz_ADN(dimension);
}
	

int main()
{
	tamano_matriz();
	
	return 0;
}

