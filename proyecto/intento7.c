#include <stdio.h>
#include <stdlib.h>	
#include <time.h>

void imprimir_secuencia(int lado, char adn[lado][lado]){
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			printf("|%c|", adn[i][j]);
		}
		printf("\n");
	}
}
char letra_nueva(char nucleotido){
	switch(nucleotido){
		case 'A':
		return 'T';
		break;
							
		case 'C':
		return 'G';
		break;
							
		case 'G':
		return 'C';
		break;
							
		case 'T':
		return 'A';
		break;
	}
}

char cambio_secuencia(int lado, char adn[lado][lado], int mutaciones[lado][lado], int vecino[lado][lado]){
	char nueva_secuencia[lado][lado];
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			if(vecino[i][j]==3){
				nueva_secuencia[i][j]=letra_nueva(adn[i][j]);
				printf("Posición (%d, %d) presenta mutación (cambio de letra)\n", i, j);
			}
			else{
				if(mutaciones[i][j]>=1){
					nueva_secuencia[i][j]=adn[i][j];
					printf("Posición (%d, %d) presenta mutación (se mantiene)\n", i, j);
				}
				else{
					nueva_secuencia[i][j]="ACGT"[rand()%4];
				}
			}
		}
	}
	imprimir_secuencia(lado, nueva_secuencia);
	return nueva_secuencia[lado][lado];
}

int mut_columna(int lado, int i, int j, char adn[lado][lado]){
	int contador;
	if(adn[i][j]=='T'){
		int k=j+1;
		int vecinos=0;
		contador=0;
		while(k<lado){
			if(vecinos<4){
				if(adn[i][k]==adn[i][j]){
					contador++;
				}
				vecinos++;
			}
			k++;
		}
	}
	else if(adn[i][j]=='C'){
		int k=j-1;
		int vecinos=0;
		contador=0;
		while(k>=0){
			if(vecinos<4){
				if(adn[i][k]==adn[i][j]){
					contador++;
				}
				vecinos++;
			}
			k--;
		}
	}
	return contador;
}

int mut_fila(int lado, int i, int j, char adn[lado][lado]){
	int contador;
	if(adn[i][j]=='G'){
		int k=i+1;
		int vecinos=0;
		contador=0;
		while(k<lado){
			if(vecinos<4){
				if(adn[k][j]==adn[i][j]){
					contador++;
				}
				vecinos++;
			}
			k++;
		}
	}
	else if(adn[i][j]=='A'){
		int k=i-1;
		int vecinos=0;
		contador=0;
			while(k>=0){
				if(vecinos<4){
					if(adn[k][j]==adn[i][j]){
						contador++;
					}
					vecinos++;
				}
				k--;
			}
		}
	return contador;
}
//función que realiza las mutaciones
char mutaciones_siguiente(int lado, int vecino[lado][lado], char adn[lado][lado]){
	int mutacion[lado][lado]; //mutacion por cada letra
	char nueva_secuencia[lado][lado];
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
				if(adn[i][j]=='T' || adn[i][j]=='C'){
					mutacion[i][j]=mut_columna(lado, i, j, adn);
				}
				else if(adn[i][j]=='A' || adn[i][j]=='G'){
					mutacion[i][j]=mut_fila(lado, i, j, adn);
					}
	}
}
	
	nueva_secuencia[lado][lado]=cambio_secuencia(lado, adn, mutacion, vecino);
	return nueva_secuencia[lado][lado];
	
}

//función que verifica los vecinos según con lo que se llama la función
int cuantos_vecinos(char nucleotido, char vecino, char buscado, int nvecino){
	//si el vecino es el que se busca, se retorna la cantidad de vecinos+1
	if(vecino==buscado){
		return nvecino+1;
	}
	else{
		return nvecino;
	}
}

//función que realiza la primera mutación
char mutaciones_uno(int lado, int generaciones, char adn[lado][lado]){
	int vecinos, vecino[lado][lado]; //variables
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			/*se valorizan con 0 debido a que son contadores
			 * hay dos debido a que una es para cada número de la matriz y la otra es general*/
			vecino[i][j]=0; 
			vecinos=0;
			/*se dan nuevos valores en ciclos (k y l) para que recorra los alrededores del número correspondiente*/
			for(int k=i-1; k<=i+1; k++){
				for(int l=j-1; l<=j+1; l++){
					/*condición para valores de dimension no existentes como el -1 o un número mayor al de la dimensión
					 * mediante esta se va llamando a la función que calcula los vecinos
					 * dependiendo de la letra evaluada se envían los valores a la misma función*/
					if((k>=0 && k<lado)&&(l>=0 && l<lado)){
						switch(adn[i][j]){
							case 'A':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'G', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'C':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'T', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'G':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'A', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'T':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'C', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
						}
					}
					else{
						vecino[i][j]=vecinos;
					}
				}
			}
		}
	}
	printf("\n");
	//llamado a función de la segunda mutación y que realiza las mutaciones
	return mutaciones_siguiente(lado, vecino, adn);
}

//función que realiza la matriz original 
void secuencia_molde(int lado, int generaciones){
	//semilla de tiempo para la no redudancia de valores aleatorios
	srand(time(0));
	
	char adn[lado][lado]; //matriz
	
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			//se declaran las letras previas para la aleatoriedad
			adn[i][j]="ACGT"[rand()%4];
		}
	}
	
	//se imprime la secuencia
	imprimir_secuencia(lado, adn);
	for(int h=1; h<=generaciones; h++){
		printf("Generación %d.\n", h);
		adn[lado][lado]=mutaciones_uno(lado, generaciones, adn);
		imprimir_secuencia(lado, adn);
		}
}

//función que solicita las generaciones
int generacion(){
	int generaciones;
	
	printf("Por favor, ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	return generaciones;
}

//función que solicita la dimension
int dimension(){
	int dimensiones;
	
	printf("Por favor, ingrese la dimension de secuencia: ");
	scanf("%d", &dimensiones);
	
	return dimensiones;
}


int main()
{
	int dimensiones, generaciones;
	
	//valorización de variableses mediante función
	dimensiones=dimension();
	generaciones=generacion();
	
	//llamado a la función para la secuencia original
	secuencia_molde(dimensiones, generaciones);
	//subsecuencia=subsecuencia();
	
	//menu(generaciones, dimensiones);
	
	return 0;
}

