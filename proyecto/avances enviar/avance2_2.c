#include <stdio.h>
#include <stdlib.h>	
#include <time.h>

void imprimir_secuencia(int lado, int hijo, char adn[lado][lado][hijo]){
	printf("\nGeneración %d:\n", hijo);
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			printf("|%c|", adn[i][j][hijo]);
		}
		printf("\n");
	}
}

char letra_nueva(char nucleotido){
	switch(nucleotido){
		case 'A':
		return 'T';
		break;
							
		case 'C':
		return 'G';
		break;
							
		case 'G':
		return 'C';
		break;
							
		case 'T':
		return 'A';
		break;
	}
	return nucleotido;
}

char cambio_secuencia(int lado, int hijo, char adn[lado][lado][hijo], int mutaciones[lado][lado], int vecino[lado][lado]){
	char nueva_secuencia[lado][lado][hijo];
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			if(vecino[i][j]==3){
				nueva_secuencia[i][j][hijo]=letra_nueva(adn[i][j][hijo]);
			}
			else{
				if(mutaciones[i][j]>=1){
					nueva_secuencia[i][j][hijo]=adn[i][j][hijo];
				}
				else{
					if(hijo>1){
						nueva_secuencia[i][j][hijo]="ACGT"[rand()%4];
					}
					else{
						nueva_secuencia[i][j][hijo]="ACGT"[rand()%4];
					}
				}
			}
		}
	}
	//se imprime
	imprimir_secuencia(lado, hijo, nueva_secuencia);
	return nueva_secuencia[lado][lado][hijo];
}
//funcion que evalua para las últimas reglas de mutación, cuando se evalúan las columnas
int mut_columna(int lado, int hijo, int i, int j, char adn[lado][lado][hijo]){
	int contador, nucleotidos;
	if(adn[i][j][hijo]=='T'){
		int k=j+1;
		nucleotidos=0;
		contador=0;
		while(k<lado){
			if(nucleotidos<4){
				if(adn[i][k][hijo]==adn[i][j][hijo]){
					contador++;
				}
				nucleotidos++;
			}
			k++;
		}
	}
	else if(adn[i][j][hijo]=='C'){
		int k=j-1;
		nucleotidos=0;
		contador=0;
		while(k>=0){
			if(nucleotidos<4){
				if(adn[i][k][hijo]==adn[i][j][hijo]){
					contador++;
				}
				nucleotidos++;
			}
			k--;
		}
	}
	return contador;
}
//funcion que evalua para las últimas reglas de mutación, cuando se evalúan las filas
int mut_fila(int lado, int hijo, int i, int j, char adn[lado][lado][hijo]){
	int contador, nucleotidos;
	
	if(adn[i][j][hijo]=='G'){
		int k=i+1;
		nucleotidos=0;
		contador=0;
		while(k<lado){
			if(nucleotidos<4){
				if(adn[k][j][hijo]==adn[i][j][hijo]){
					contador++;
				}
				nucleotidos++;
			}
			k++; 
		}
	}
	else if(adn[i][j][hijo]=='A'){
		int k=i-1;
		nucleotidos=0;
		contador=0;
			while(k>=0){
				if(nucleotidos<4){
					if(adn[k][j][hijo]==adn[i][j][hijo]){
						contador++;
					}
					nucleotidos++;
				}
				k--;
			}
		}
	return contador;
}
//función que analiza las últimas 4 reglas de mutación
char mutacion_siguiente(int lado, int hijo, int vecino[lado][lado], char adn[lado][lado][hijo]){
	int mutacion[lado][lado]; //mutacion por cada letra
	char secuencia[lado][lado][hijo];
	
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			if(adn[i][j][hijo]=='T' || adn[i][j][hijo]=='C'){
				mutacion[i][j]=mut_columna(lado, hijo, i, j, adn);
			}
			else if(adn[i][j][hijo]=='A' || adn[i][j][hijo]=='G'){
				mutacion[i][j]=mut_fila(lado, hijo, i, j, adn);
			}
		}
	}
	secuencia[lado][lado][hijo]=cambio_secuencia(lado, hijo, adn, mutacion, vecino);
	return secuencia[lado][lado][hijo];
}
//función que cuenta los vecinos buscados
int cuantos_vecinos(char nucleotido, char vecino, char buscado, int nvecino){
	if(vecino==buscado){
		return nvecino+1;
	}
	else{
		return nvecino;
	}
}
char mutaciones_uno(int lado, int hijo, char adn[lado][lado][hijo]){
	int vecinos, vecino[lado][lado]; //variables
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			vecino[i][j]=0; 
			vecinos=0;
			for(int k=i-1; k<=i+1; k++){
				for(int l=j-1; l<=j+1; l++){
					if((k>=0 && k<lado)&&(l>=0 && l<lado)){
						switch(adn[i][j][hijo]){
							case 'A':
							vecinos=cuantos_vecinos(adn[i][j][hijo], adn[k][l][hijo], 'G', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'C':
							vecinos=cuantos_vecinos(adn[i][j][hijo], adn[k][l][hijo], 'T', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'G':
							vecinos=cuantos_vecinos(adn[i][j][hijo], adn[k][l][hijo], 'A', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'T':
							vecinos=cuantos_vecinos(adn[i][j][hijo], adn[k][l][hijo], 'C', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
						}
					}
					else{
						vecino[i][j]=vecinos;
					}
				}
			}
		}
	}
	printf("\n");
	//llamado a función de la segunda mutación y que realiza las mutaciones
	adn[lado][lado][hijo]=mutacion_siguiente(lado, hijo, vecino, adn);
	return adn[lado][lado][hijo];
}

char secuencia_molde(int lado, int hijo){
	char adn[lado][lado][hijo]; //matriz
	//semilla de tiempo para la no redudancia de valores aleatorios
	srand(time(0));
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			//se declaran las letras previas para la aleatoriedad
			adn[i][j][hijo]="ACGT"[rand()%4];
		}
	}
	
	//se imprime
	imprimir_secuencia(lado, hijo, adn);
	return adn[lado][lado][hijo];
}

//función que solicita las generaciones
int generacion(){
	int generaciones;
	
	printf("Por favor, ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	return generaciones;
}

//función que solicita la dimension
int dimension(){
	int dimensiones;
	
	printf("Por favor, ingrese la dimension de secuencia: ");
	scanf("%d", &dimensiones);
	
	return dimensiones;
}

int main()
{
	int dimensiones, generaciones;
	
	printf("Se verá una secuencia molde y se realizaran mutaciones por generación. \n");
	
	//valorización de variableses mediante función
	dimensiones=dimension();
	generaciones=generacion();
	char secuencia[dimensiones][dimensiones][generaciones];
	
	for(int i=0; i<=generaciones; i++){
		if(i==0){
			secuencia[dimensiones][dimensiones][i]=secuencia_molde(dimensiones, i);
		}
		else{
			secuencia[dimensiones][dimensiones][i]=mutaciones_uno(dimensiones, i, secuencia);
		}
	}
	
	return 0;
}
