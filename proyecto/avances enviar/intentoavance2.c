#include <stdio.h>
#include <stdlib.h>	
#include <time.h>
void imprimir_secuencia(int generacion, int lado, char adn[generacion][lado][lado]){
	printf("Generación %d: \n", generacion);
		for(int i=0; i<lado; i++){
			for(int j=0; j<lado; j++){
				printf("|%c|", adn[generacion][i][j]);
				}
			printf("\n");
	}
}

int mutacion_fila(int generacion, int lado, int i, int j, char adn[generacion][lado][lado]){
	int contador, nucleotidos;
	if(adn[generacion][i][j]=='G'){
		int k=i+1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[generacion][k][j]==adn[generacion][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[generacion][i][j]=='C'){
		int k=i-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[generacion][k][j]==adn[generacion][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se suma para evaluar el nucleotido siguiente
		}
	}
	return contador;
}
int mutacion_columna(int generacion, int lado, int i, int j, char adn[generacion][lado][lado]){
	int contador, nucleotidos;
	if(adn[generacion][i][j]=='T'){
		int k=j+1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[generacion][i][k]==adn[generacion][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[generacion][i][j]=='C'){
		int k=j-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[generacion][i][k]==adn[generacion][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se suma para evaluar el nucleotido siguiente
		}
	}
	return contador;
}

int cuantos_alrededor(int generacion, int lado, char adn[generacion][lado][lado]){
	int mutacion[lado][lado];
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			if(adn[generacion][i][j]=='T' || adn[generacion][i][j]=='C'){
				mutacion[i][j]=mutacion_columna(generacion, lado, i, j, adn);
			}
			else if(adn[generacion][i][j]=='A' || adn[generacion][i][j]=='G'){
				mutacion[i][j]=mutacion_fila(generacion, lado, i, j, adn);
			}
		}
	}
	return mutacion[lado][lado];
}

int contar_vecinos(char vecino, char buscado, int nvecino){
	if(vecino==buscado){
		return nvecino+1;
	}
	else{
		return nvecino;
	}
	return nvecino;
}

char cambio_letra(char nucleotido){
	switch(nucleotido){
		case 'A':
		return 'T';
		break;
		case 'C':
		return 'G';
		break;
		case 'G':
		return 'C';
		break;
		case 'T':
		return 'A';
		break;
	}
	return nucleotido;
}
int cuantos_vecinos(int generacion, int lado, char adn[generacion][lado][lado]){
	int vecino, vecinos[lado][lado];
	for(int i=0; i<lado; i++){
			for(int j=0; j<lado; j++){
				vecino=0;
				vecinos[i][j]=0;
				for(int k=i-1; k<=i+1; k++){
					for(int l=j-1; l<=j+1; l++){
						if((k>=0 && k<lado)&&(l>=0 && l<lado)){
							switch(adn[generacion][i][j]){
								case 'A':
								vecino=contar_vecinos(adn[generacion][k][l], 'G', vecinos[i][j]);
								vecinos[i][j]=vecino;
								break;
								case 'C':
								vecino=contar_vecinos(adn[generacion][k][l], 'T', vecinos[i][j]);
								vecinos[i][j]=vecino;
								break;
								case 'G':
								vecino=contar_vecinos(adn[generacion][k][l], 'A', vecinos[i][j]);
								vecinos[i][j]=vecino;
								break;
								case 'T':
								vecino=contar_vecinos(adn[generacion][k][l], 'C', vecinos[i][j]);
								vecinos[i][j]=vecino;
								break;
							}
						}
						else{
							vecinos[i][j]=vecino;
						}
					}
				}
			}
		}
		return vecinos[lado][lado];
}

char cambio_secuencia(int generacion, int lado, char adn[generacion][lado][lado]){
	int vecino[lado][lado], mutacion[lado][lado];
	
	vecino[lado][lado]=cuantos_vecinos(generacion, lado, adn);
	mutacion[lado][lado]=cuantos_alrededor(generacion, lado, adn);
	
	for(int i=0; i<lado; i++){
			for(int j=0; j<lado; j++){
				if(vecino[i][j]==3){
					adn[generacion+1][i][j]=cambio_letra(adn[generacion][i][j]);
				}
				else{
					if(mutacion[i][j]>=1){
						adn[generacion+1][i][j]=adn[generacion][i][j];
					}
					else{
						//frecuencia  a usar aun no hecha
						adn[generacion+1][i][j]="ACGT"[rand()%4];
					}
				}
				printf("|%c|", adn[generacion+1][i][j]);
			}
			printf("\n");
		}
		return adn[generacion+1][lado][lado];	
}

char secuencia_hija(int generacion, int lado, char adn[generacion-1][lado][lado]){
	adn[generacion][lado][lado]=cambio_secuencia(generacion-1, lado, adn);
	return adn[generacion][lado][lado];
}
char secuencia_molde(int lado){
	srand(time(0));
	char adn[0][lado][lado];
	for(int i=0; i<lado; i++){
			for(int j=0; j<lado; j++){
				adn[0][i][j]="ACGT"[rand()%4];
				printf("|%c|", adn[0][i][j]);
			}
			printf("\n");
		}
	return adn[0][lado][lado];
}

void secuencia(int lado, int generacion){
	char adn[generacion][lado][lado];
	adn[0][lado][lado]=secuencia_molde(lado);
	imprimir_secuencia(0, lado, adn);
	
	//~ for(int k=1; k<=generacion; k++){
		//~ adn[k][lado][lado]=secuencia_hija(k, lado, adn);
		//~ imprimir_secuencia(k, lado, adn);
	//~ }
	
}
int dimensiones(){
	int dimension;
	
	printf("Por favor, ingrese la dimension de secuencia: ");
	scanf("%d", &dimension);
	
	return dimension;
}

int generacion(){
	int generaciones;
	
	printf("Por favor, ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	return generaciones;
}

void realizacion_matrices(){
	int dimension, generaciones;
	
	printf("Se verá una secuencia molde y se realizaran mutaciones por generación. \n");
	
	//valorización de variableses mediante función
	dimension=dimensiones();
	generaciones=generacion();
	
	secuencia(dimension, generaciones);
}

int main()
{
	realizacion_matrices();
	
	return 0;
}

