#include <stdio.h>
#include <stdlib.h>	
#include <time.h>

void imprimir_secuencia(int lado, char adn[lado][lado], int generaciones){
	printf("\nGeneración %d:\n", generaciones);
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			printf("|%c|", adn[i][j]);
		}
		printf("\n");
	}
}

char letra_nueva(char nucleotido){
	switch(nucleotido){
		case 'A':
		return 'T';
		break;
							
		case 'C':
		return 'G';
		break;
							
		case 'G':
		return 'C';
		break;
							
		case 'T':
		return 'A';
		break;
	}
	return nucleotido;
}

char cambio_secuencia(int lado, char adn[lado][lado], int mutaciones[lado][lado], int vecino[lado][lado], int generaciones){
	char nueva_secuencia[lado][lado];
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			if(vecino[i][j]==3){
				nueva_secuencia[i][j]=letra_nueva(adn[i][j]);
			}
			else{
				if(mutaciones[i][j]>=1){
					nueva_secuencia[i][j]=adn[i][j];
				}
				else{
					if(generaciones>1){
						nueva_secuencia[i][j]="ACGT"[rand()%4];
					}
					else{
						nueva_secuencia[i][j]="ACGT"[rand()%4];
					}
				}
			}
		}
	}
	//se imprime
	imprimir_secuencia(lado, nueva_secuencia, generaciones);
	return nueva_secuencia[lado][lado];
}

//funcion que evalua para las últimas reglas de mutación, cuando se evalúan las columnas
int mut_columna(int lado, int i, int j, char adn[lado][lado]){
	int contador, nucleotidos;
	if(adn[i][j]=='T'){
		int k=j+1;
		nucleotidos=0;
		contador=0;
		while(k<lado){
			if(nucleotidos<4){
				if(adn[i][k]==adn[i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;
		}
	}
	else if(adn[i][j]=='C'){
		int k=j-1;
		nucleotidos=0;
		contador=0;
		while(k>=0){
			if(nucleotidos<4){
				if(adn[i][k]==adn[i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;
		}
	}
	return contador;
}
//funcion que evalua para las últimas reglas de mutación, cuando se evalúan las filas
int mut_fila(int lado, int i, int j, char adn[lado][lado]){
	int contador, nucleotidos;
	
	if(adn[i][j]=='G'){
		int k=i+1;
		nucleotidos=0;
		contador=0;
		while(k<lado){
			if(nucleotidos<4){
				if(adn[k][j]==adn[i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++; 
		}
	}
	else if(adn[i][j]=='A'){
		int k=i-1;
		nucleotidos=0;
		contador=0;
			while(k>=0){
				if(nucleotidos<4){
					if(adn[k][j]==adn[i][j]){
						contador++;
					}
					nucleotidos++;
				}
				k--;
			}
		}
	return contador;
}
//función que analiza las últimas 4 reglas de mutación
char mutacion_siguiente(int lado, int vecino[lado][lado], char adn[lado][lado], int generaciones){
	int mutacion[lado][lado]; //mutacion por cada letra
	char secuencia[lado][lado];
	
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			if(adn[i][j]=='T' || adn[i][j]=='C'){
				mutacion[i][j]=mut_columna(lado, i, j, adn);
			}
			else if(adn[i][j]=='A' || adn[i][j]=='G'){
				mutacion[i][j]=mut_fila(lado, i, j, adn);
			}
		}
	}
	secuencia[lado][lado]=cambio_secuencia(lado, adn, mutacion, vecino, generaciones);
	return secuencia[lado][lado];
}

//función que cuenta los vecinos buscados
int cuantos_vecinos(char nucleotido, char vecino, char buscado, int nvecino){
	if(vecino==buscado){
		return nvecino+1;
	}
	else{
		return nvecino;
	}
}

//función que analiza las primeras 4 reglas de mutación
char mutaciones_uno(int lado, char adn[lado][lado], int generaciones){
	int vecinos, vecino[lado][lado]; //variables
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			vecino[i][j]=0; 
			vecinos=0;
			for(int k=i-1; k<=i+1; k++){
				for(int l=j-1; l<=j+1; l++){
					if((k>=0 && k<lado)&&(l>=0 && l<lado)){
						switch(adn[i][j]){
							case 'A':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'G', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'C':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'T', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'G':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'A', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'T':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'C', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
						}
					}
					else{
						vecino[i][j]=vecinos;
					}
				}
			}
		}
	}
	printf("\n");
	//llamado a función de la segunda mutación y que realiza las mutaciones
	adn[lado][lado]=mutacion_siguiente(lado, vecino, adn, generaciones);
	return adn[lado][lado];
}

//función que realiza la matriz original 
char secuencia_molde(int lado, int generaciones){
	char nueva_secuencia[lado][lado];
	//semilla de tiempo para la no redudancia de valores aleatorios
	srand(time(0));
	
	char adn[lado][lado]; //matriz
	if(generaciones==0){
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			//se declaran las letras previas para la aleatoriedad
			adn[i][j]="ACGT"[rand()%4];
		}
	}
	
	//se imprime
	imprimir_secuencia(lado, adn, generaciones);
	return adn[lado][lado];
	}
	
	else{
	//se realizan las mutaciones
	nueva_secuencia[lado][lado]=mutaciones_uno(lado, adn, generaciones);
	}
	return nueva_secuencia[lado][lado];
}

//función que solicita las generaciones
int generacion(){
	int generaciones;
	
	printf("Por favor, ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	return generaciones;
}

//función que solicita la dimension
int dimension(){
	int dimensiones;
	
	printf("Por favor, ingrese la dimension de secuencia: ");
	scanf("%d", &dimensiones);
	
	return dimensiones;
}


int main()
{
	int dimensiones, generaciones;
	
	printf("Se verá una secuencia molde y se realizaran mutaciones por generación. \n");
	
	//valorización de variableses mediante función
	dimensiones=dimension();
	generaciones=generacion();
	char secuencia[dimensiones][dimensiones][generaciones];
	
	//llamado a la función para la secuencia origial
	for(int i=0; i<=generaciones; i++){
		secuencia[i][dimensiones][dimensiones]=secuencia_molde(dimensiones, i);
	}
	
	return 0;
}
