/*No es el proyecto completo, solo incluye la realización de mutaciones de la secuencia origial a la padre.
 * Sin embargo, presenta un menú y una función de generaciones.
 * Ambas están comentadas por completo para que no realicen cambios dentro del programa actual.*/
 
 #include <stdio.h>
#include <stdlib.h>	
#include <time.h>
 
 //funcion que imprime las secuencias
void imprimir_secuencia(int lado, char adn[lado][lado]){
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			printf("|%c|", adn[i][j]);
		}
		printf("\n");
	}
}
//funcion que cambia la letra por su complementaria
char letra_nueva(char nucleotido){
	switch(nucleotido){
		case 'A':
		return 'T';
		break;
							
		case 'C':
		return 'G';
		break;
							
		case 'G':
		return 'C';
		break;
							
		case 'T':
		return 'A';
		break;
	}
	/*debido a que no es una función void, marca warning al no retornar nada fuera del switch
	 * por lo que si no entra al switch, retorna el nucleotido ingresado
	 * a pesar de que no se ingresa un caracter que no entre en el switch*/
	return nucleotido;
}

//funcion que realiza las mutaciones
void cambio_secuencia(int lado, char adn[lado][lado], int mutaciones[lado][lado], int vecino[lado][lado]){
	//matriz de la nueva secuencia
	char nueva_secuencia[lado][lado];
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			//si los vecinos son 3, se realiza una de las primeras 4 reglas
			if(vecino[i][j]==3){
				nueva_secuencia[i][j]=letra_nueva(adn[i][j]);
			}
			else{
				//si se repite la letra dentro del rango evaluado, en las últimas mutaciones, se realiza la mutación de mantener la letra
				if(mutaciones[i][j]>=1){
					nueva_secuencia[i][j]=adn[i][j];
				}
				//sino, es aleatorio
				else{
					nueva_secuencia[i][j]="ACGT"[rand()%4];
				}
			}
		}
	}
	//se imprime
	printf("Generación 1: \n");
	imprimir_secuencia(lado, nueva_secuencia);
}

//funcion que evalua para las últimas reglas de mutación, cuando se evalúan las columnas
int mut_columna(int lado, int i, int j, char adn[lado][lado]){
	/*contador para ver cuantas veces está el mismo nucleótido
	 * nucleotidos es contador para que se evaluen solo 4*/
	int contador, nucleotidos;
	if(adn[i][j]=='T'){
		/*k para evaluar el nucleotido correspondiente
		 se evalúan las columnas siguientes, por eso es j+1*/
		int k=j+1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[i][k]==adn[i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[i][j]=='C'){
		/*k para evaluar el nucleotido correspondiente
		 se evalúan las columnas anteriores, por eso es j-1*/
		int k=j-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea mayor al mínimo de largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[i][k]==adn[i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se resta para evaluar el nucleotido siguiente
		}
	}
	return contador;
}
//funcion que evalua para las últimas reglas de mutación, cuando se evalúan las filas
int mut_fila(int lado, int i, int j, char adn[lado][lado]){
	/*contador para ver cuantas veces está el mismo nucleótido
	 * nucleotidos es contador para que se evaluen solo 4*/
	int contador, nucleotidos;
	
	if(adn[i][j]=='G'){
		/*k para evaluar el nucleotido correspondiente
		 se evalúan las filas siguintes, por eso es i+1*/
		int k=i+1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[k][j]==adn[i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++; //se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[i][j]=='A'){
		/*k para evaluar el nucleotido correspondiente
		 se evalúan las filas anteriores, por eso es i-1*/
		int k=i-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea mayor al mínimo del largo de la matriz
			while(k>=0){
				if(nucleotidos<4){
					if(adn[k][j]==adn[i][j]){
						contador++;
					}
					nucleotidos++;
				}
				k--;//se resta para evaluar el nucleotido siguiente
			}
		}
	return contador;
}
//función que analiza las últimas 4 reglas de mutación
void mutacion_siguiente(int lado, int vecino[lado][lado], char adn[lado][lado]){
	int mutacion[lado][lado]; //mutacion por cada letra
	
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			/*cuando son pirimidinas se evaluan por columna
			 * cuando son purinas se evaluan por fila
			 * se llama a funcion que evalua la mutación segun lo que se debe evaluar*/
			if(adn[i][j]=='T' || adn[i][j]=='C'){
				mutacion[i][j]=mut_columna(lado, i, j, adn);
			}
			else if(adn[i][j]=='A' || adn[i][j]=='G'){
				mutacion[i][j]=mut_fila(lado, i, j, adn);
			}
		}
	}
	//llamado a funcion que realiza las mutaciones en la secuencia madre/padre
	cambio_secuencia(lado, adn, mutacion, vecino);
}

//función que cuenta los vecinos buscados
int cuantos_vecinos(char nucleotido, char vecino, char buscado, int nvecino){
	//si el vecino es el que se busca, se retorna la cantidad de vecinos+1
	if(vecino==buscado){
		return nvecino+1;
	}
	else{
		return nvecino;
	}
}

//función que analiza las primeras 4 reglas de mutación
void mutaciones_uno(int lado, char adn[lado][lado]){
	int vecinos, vecino[lado][lado]; //variables
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			/*se valorizan con 0 debido a que son contadores
			 * hay dos debido a que una es para cada número de la matriz y la otra es general*/
			vecino[i][j]=0; 
			vecinos=0;
			/*se dan nuevos valores en ciclos (k y l) para que recorra alrededores del número correspondiente*/
			for(int k=i-1; k<=i+1; k++){
				for(int l=j-1; l<=j+1; l++){
					/*condición para valores de dimension no existentes como el -1 o un número mayor al de la dimensión
					 * mediante esta se va llamando a la función que calcula los vecinos
					 * dependiendo de la letra evaluada se envían los valores a la misma función*/
					if((k>=0 && k<lado)&&(l>=0 && l<lado)){
						switch(adn[i][j]){
							case 'A':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'G', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'C':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'T', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'G':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'A', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
							
							case 'T':
							vecinos=cuantos_vecinos(adn[i][j], adn[k][l], 'C', vecino[i][j]);
							vecino[i][j]=vecinos;
							break;
						}
					}
					else{
						vecino[i][j]=vecinos;
					}
				}
			}
		}
	}
	printf("\n");
	//llamado a función de la segunda mutación y que realiza las mutaciones
	mutacion_siguiente(lado, vecino, adn);
}

//función que realiza la matriz original 
void secuencia_molde(int lado){
	//semilla de tiempo para la no redudancia de valores aleatorios
	srand(time(0));
	
	char adn[lado][lado]; //matriz
	
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			//se declaran las letras previas para la aleatoriedad
			adn[i][j]="ACGT"[rand()%4];
		}
	}
	
	//se imprime
	imprimir_secuencia(lado, adn);
	//se realizan las mutaciones
	mutaciones_uno(lado, adn);
}

/*FUNCIÓN DE GENERACIONES COMENTADA PARA QUE NO REALICE CAMBIOS DEBIDO A QUE AÚN NO SE USA
//función que solicita las generaciones
int generacion(){
	int generaciones;
	
	printf("Por favor, ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	return generaciones;
}*/


//función que solicita la dimension
int dimension(){
	int dimensiones;
	
	printf("Por favor, ingrese la dimension de secuencia: ");
	scanf("%d", &dimensiones);
	
	return dimensiones;
}


int main()
{
	int dimensiones; //generaciones;
	
	printf("Se verá una secuencia molde y se realizaran mutaciones por generación. \n");
	
	//valorización de variableses mediante función
	dimensiones=dimension();
	/*generaciones=generacion();
	 * aun no se utiliza*/
	
	//llamado a la función para la secuencia origial
	printf("Generación 0: \n");
	secuencia_molde(dimensiones);
	
	return 0;
}
