#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
//imprime las matrices
void imprimir_secuencia(int lado, int hijo, char adn[hijo][lado][lado]){
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			printf("|%c|", adn[hijo][i][j]);
		}
		printf("\n");
	}
	printf("\n");
}
//imprime los aminoácidos
void imprimir_aminoacidos(int codones, char aa[codones]){
	printf("\nLos aminoácidos codificantes por la secuencia ingresada son: ");
	for(int i=0; i<codones; i++){
		printf("|%c|", aa[i]);
	}
}
//imprime coordenadas donde se encontró la secuencia en el genoma padre
void imprimir_encuentros(int veces, int encontrado_i[veces], int encontrado_j[veces]){
	printf("\n-----------------------------------------------------------------");
	printf("\nEncontrada en: ");
	for(int k=0; k<veces; k++){
		printf("[%d, %d]", encontrado_i[k], encontrado_j[k]);
	}
}
//transforma la secuencia en aminoacidos
char transformar_aa(int codones, char amino[codones][3], int i){
	char combinaciones[64][3]={ "AAA", "AAC", "AAG", "AAT", "ACA", "ACC", "ACG", "ACT", "AGA", "AGC", "AGG", "AGT", "ATA", "ATC", "ATG", "ATT", "CAA", "CAC", "CAG", "CAT", "CCA", "CCC", "CCG", "CCT", "CGA", "CGC", "CGG", "CGT", "CTA", "CTC", "CTG", "CTT", "GAA", "GAC", "GAG", "GAT", "GCA", "GCC", "GCG", "GCT", "GGA", "GGC", "GGG", "GGT", "GTA", "GTC", "GTG", "GTT", "TAA", "TAC", "TAG", "TAT", "TCA", "TCC", "TCG", "TCT", "TGA", "TGC", "TGG", "TGT", "TTA", "TTC", "TTG", "TTT" };
	char aminoacido[64]={ "KNKNTTTTRSRSIIMIQHQHPPPPRRRRLLLLEDEDAAAAGGGGVVVV-Y-YSSSS-CWCLFLF" };
	int contador=0, encontrado;
		for(int j=0; j<64; j++){
			for(int k=0; k<3; k++){
				if(combinaciones[j][k]==amino[i][k]){
					contador++;
				}
				else{
					contador=0;
				}
			}
			//si coincide en los 3 nucleotidos de un codón, se guarda el número de codón identificado
			if(contador==3){
				encontrado=j;
			}
		}
		return aminoacido[encontrado];
}
//divide la secuencia en codones
char encontrar_aminoacido(int codones, int largo, char secuencia[largo], int l){
	//secuencia dividido en arreglo de dos dimensiones
	char amino[codones][3];
	int k=0;
	for(int i=0; i<codones; i++){
		for(int j=0; j<3; j++){
			amino[i][j]=secuencia[k];
			k++;
		}
	}
	return transformar_aa(codones, amino, l);
}
//analiza que tipo de mutación es
void que_es(int codones, char aa[codones], char aa_2[codones]){
	for(int i=0; i<codones; i++){
		//si los aminoacidos son iguales, es sinónima
		if(aa[i]==aa_2[i]){
			printf("Es una mutación sinónima: |%c|\n", aa_2[i]);
		}
		else{
			printf("Es una mutación no sinónima: |%c|\n", aa_2[i]);
		}
	}
}
//convierte la secuencia en aminoacido
void analisis_mutacion(int largo, char secuencia[largo], int codones, char aa[codones]){
	char aa_evaluar[codones];
	for(int i=0; i<codones; i++){
		aa_evaluar[i]=encontrar_aminoacido(codones, largo, secuencia, i);
	}
	que_es(codones, aa, aa_evaluar);
}
//convierte las coordenadas encontradas en la generacion padre, pero en las demás generaciones, en una secuencia
void mutacion_aa(int lado, int generaciones, char adn[generaciones][lado][lado], int largo, int codones, char aa[codones], int en_i[largo], int en_j[largo]){
	char secuencia[largo];
	for(int h=2; h<=generaciones; h++){
		printf("En generacion %d: ", h);
		for(int i=0; i<largo; i++){
			secuencia[i]=adn[h][en_i[i]][en_j[i]];
		}
		analisis_mutacion(largo, secuencia, codones, aa);
	}
}
//separa la secuencia ingresada en codones
int contar_codones(int largo){
	//variables
	int codones, resto;
	/*los codones son tripletes por lo que si el total de la secuencia no es un múltiplo de 3, el resto no codifica nada*/
	resto=largo%3;
	codones=(largo-resto)/3;
	if(largo<3){
		printf("\nLos nucleotidos ingresados no alcanzan a completar un codón para un aminoácido.\n");
		return 0;
	}
	else{
		printf("\nHay %d nucleotidos que no completan un codón.\n", resto);
		return codones;
	}
}
//ARREGLAR ESTO
void evaluar_mutacion(int generaciones, int contador_g[generaciones]){
	int coincidencias=0, mutaciones=0;
	//se acumulan las coincidencias
	for(int h=2; h<=generaciones; h++){
		coincidencias=coincidencias+contador_g[h];
	}
	//la cantidad de mutaciones es las generaciones (menos la del padre) menos la cantidad de coincidencias
	mutaciones=(generaciones-1)-coincidencias;
	printf("\nA través de las generaciones las posiciones encontradas mutaron %d veces.", mutaciones);
	printf("\nSe encontraron %d coincidencias a través de las generaciones.\n", coincidencias);
}
//ARREGLAR ESTO
void evaluar_coincidencias(int lado, int generacion, char adn[generacion][lado][lado], int largo, char secuencia[largo], int encontrado_i[largo], int encontrado_j[largo]){
	int conta_g[generacion], contador_coincidencias;
	//evalúa las generaciones siguientes para comparar
	for(int h=2; h<=generacion; h++){
		for(int i=0; i<largo; i++){
			contador_coincidencias=0;
			if(adn[1][encontrado_i[i]][encontrado_j[i]]==adn[h][encontrado_i[i]][encontrado_j[i]]){
				//si encuentra la igualdad, aumenta el contador
				contador_coincidencias++;
			}
			else{
				contador_coincidencias=0;
			}
		}
		//si el contador es igual al largo, significa que en esas coordenadas entre las generaciones son iguales
		if(contador_coincidencias==largo){
			conta_g[h]=1;
		}
		else{
			conta_g[h]=0;
		}
	}
	evaluar_mutacion(generacion, conta_g);
}
//busqueda diagonal 2
void segundadiagonal(int lado, int hijo, int generacion, char adn[hijo][lado][lado], int i, int j, int largo, char secuencia[largo], int codones, char aa[codones]){
	int contador=0, encontrado_i[largo], encontrado_j[largo];
	if(i+largo<=lado && j-largo>=-1){
		//si aumenta una coordenada, debe disminuir la siguiente para lograr buscar en diagonal
		int l=j;
		for(int k=i; k<i+largo; k++){
			if(adn[hijo][k][l]==secuencia[contador]){
				encontrado_i[contador]=k;
				encontrado_j[contador]=l;
				contador++;
			}
			l--;
		}
		if(contador==largo){
			imprimir_encuentros(contador, encontrado_i, encontrado_j);
			evaluar_coincidencias(lado, generacion, adn, largo, secuencia, encontrado_i, encontrado_j);
			//si existen codones, revisa las mutaciones a nivel aminoacídico
			if(codones>0){	
				mutacion_aa(lado, generacion, adn, largo, codones, aa, encontrado_i, encontrado_j);
			}
		}
	}
}
//busqueda diagonal 1
void diagonal(int lado, int hijo, int generacion, char adn[hijo][lado][lado], int i, int j, int largo, char secuencia[largo], int codones, char aa[codones]){
	int contador=0, encontrado_i[largo], encontrado_j[largo];
	if(i+largo<=lado && j+largo<=lado){
		//si aumenta una coordenada, debe aumentar la siguiente para lograr buscar en diagonal
		int l=j;
		for(int k=i; k<i+largo; k++){
			//si se encuentra, se guardan las coordenadas en un arreglo
			if(adn[hijo][k][l]==secuencia[contador]){
				encontrado_i[contador]=k;
				encontrado_j[contador]=l;
				contador++;
			}
			l++;
		}
		if(contador==largo){
			imprimir_encuentros(contador, encontrado_i, encontrado_j);
			evaluar_coincidencias(lado, generacion, adn, largo, secuencia, encontrado_i, encontrado_j);
			//si existen codones, revisa las mutaciones a nivel aminoacídico
			if(codones>0){	
				mutacion_aa(lado, generacion, adn, largo, codones, aa, encontrado_i, encontrado_j);
			}	
		}
	}
}
//busqueda vertical
void vertical(int lado, int hijo, int generacion, char adn[hijo][lado][lado], int i, int j, int largo, char secuencia[largo], int codones, char aa[codones]){
	//variables
	int contador=0;
	int encontrada_i[largo], encontrada_j[largo];
	/*si la coordenada donde se encuentra la primera letra más el largo de la secuencia es mayor o igual al lado
	 * entonces analizaría coordenadas que no existen*/
	if(i+largo<=lado){
		for(int k=i; k<i+largo; k++){
			//si se encuentra, aumenta el contador y se guardan las coordenadas en una variable
			if(secuencia[contador]==adn[hijo][k][j]){
				encontrada_i[contador]=k;
				encontrada_j[contador]=j;
				contador++;
			}
		}
		if(contador==largo){
			imprimir_encuentros(contador, encontrada_i, encontrada_j);
			evaluar_coincidencias(lado, generacion, adn, largo, secuencia, encontrada_i, encontrada_j);
			//si existen codones, revisa las mutaciones a nivel aminoacídico
			if(codones>0){	
				mutacion_aa(lado, generacion, adn, largo, codones, aa, encontrada_i, encontrada_j);
			}
		}
	}
}
//busqueda horizontal
void horizontal(int lado, int hijo, int generacion, char adn[hijo][lado][lado], int i, int j, int largo, char secuencia[largo], int codones, char aa[codones]){
	/*contador que cuenta cuantas veces se realiza el ciclo
	 * veces cuenta las veces que se encuentra la coincidencia*/
	int contador, veces;
	int encontrado_i[largo], encontrado_j[largo]; //coordenadas para imprimir
	contador=0;
	veces=0;
	while(contador<largo){
		//si j es mayor al largo de la matriz, se pasa a la fila siguiente
		if(j>=lado){
			j=j-lado;
			i++;
		}
		//si se encuentra, se guardan las variables y aumenta j para evaluar la siguiente
		if(secuencia[contador]==adn[hijo][i][j]){
			encontrado_i[veces]=i;
			encontrado_j[veces]=j;
			veces++;
			j++;
		}
		else{
			veces=0;
		}
		//condición para que no busque fuera de la matriz
		if(i>=lado){
			veces=0;
		}
		contador++;
	}
	//si se encuentra tantas veces como el largo, se encontró la secuencia
	if(veces==largo){
		imprimir_encuentros(veces, encontrado_i, encontrado_j);
		evaluar_coincidencias(lado, generacion, adn, largo, secuencia, encontrado_i, encontrado_j);
		//si existen codones, revisa las mutaciones a nivel aminoacídico
		if(codones>0){	
			mutacion_aa(lado, generacion, adn, largo, codones, aa, encontrado_i, encontrado_j);
		}
	}
}
//busca la secuencia
void encontrar_secuencia(int lado, int generacion, char adn[generacion][lado][lado], int largo, char secuencia[largo], int buscar, int codones, char aa[codones]){
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			//si encuentra una coincidencia de la primera letra, la busca en todas las direcciones
			if(secuencia[0]==adn[1][i][j]){
				horizontal(lado, 1, generacion, adn, i, j, largo, secuencia, codones, aa);
				vertical(lado, 1, generacion, adn, i, j, largo, secuencia, codones, aa);
				diagonal(lado, 1, generacion, adn, i, j, largo, secuencia, codones, aa);
				segundadiagonal(lado, 1, generacion, adn, i, j, largo, secuencia, codones, aa);
			}
		}
	}
	/*se envía un buscar para que cuando este sea uno, no se realice lo siguiente:
	 * como se busca en todas direcciones, también debe ver si hay un AGC y un CGA, por lo que la secuencia se da vuelta
	 * y cuando se da vuelta se llama a la musma función de manera recursiva con la secuencia al revés
	 * y un 1 que toma el valor de buscar, por lo que solo se analiza la secuencia normal y la que está al revés*/
	if(buscar==0){
		char sec_nueva[largo];
		int b=largo-1;
		for(int a=0; a<largo; a++){
			sec_nueva[a]=secuencia[b];
			b--;
		}
		//si la secuencia al darla vuelta es la misma, no se realiza de nuevo el proceso
		int cont=0;
		for(int c=0; c<largo; c++){
			if(sec_nueva[c]==secuencia[c]){
				cont++;
			}
		}
		if(cont!=largo){
			printf("\nEN REVERSA\n");
			encontrar_secuencia(lado, generacion, adn, largo, sec_nueva, 1, codones, aa);
		}
	}
}
/*Evalúa los codones y cambia el codon por aminoacidos; luego realiza la búsqueda de la secuencia en la generación padre*/
void analisis(int lado, int generacion, char adn[generacion][lado][lado], int largo, char secuencia[largo]){
	int codones;
	codones=contar_codones(largo);
	char aa[codones];
	for(int i=0; i<codones; i++){
		aa[i]=encontrar_aminoacido(codones, largo, secuencia, i);
	}
	imprimir_aminoacidos(codones, aa);
	encontrar_secuencia(lado, generacion, adn, largo, secuencia, 0, codones, aa);
}
char cambio_letra(char nucleotido){
	switch(nucleotido){
		case 'A': return 'T'; break;
		case 'C': return 'G'; break;
		case 'G': return 'C'; break;
		case 'T': return 'A'; break;
	}
	return nucleotido;
}
//cambia las letras de acuerdo a las mutaciones
char letra_nueva(int hijo, int lado, char adn[hijo][lado][lado], char frecuencia[lado*lado], int vecinos, int mutacion2, char nucleotido){
	//si tiene 3 correspondientes entre sus vecinos retorna el cambio de letra
	if(vecinos==3){
		return cambio_letra(nucleotido);
	}
	else{
		//si es mayor a 0, la nueva base es la misma anterior
		if(mutacion2>=1){
			return nucleotido;
		}
		else{
			//si la generacion que se está mutando es la primera (para realizar la segunda)
			if(hijo>=1){
				//se retorna una aleotoriedad respecto a las bases de la secuencia actual (para la secuencia nueva)
				return frecuencia[rand()%(lado*lado)];
			}
			else{
				return "ACGT"[rand()%4];
			}
		}
	}
}
//mutaciones de purinas
int mut_fila(int lado, int i, int j, int hijo, char adn[hijo][lado][lado]){
	int nucleotidos, contador;
	if(adn[hijo][i][j]=='G'){
		/*se deben analizar las filas siguientes
		 * por eso k=i+1*/
		int k=i+1;
		//contadores
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[hijo][k][j]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[hijo][i][j]=='A'){
		/*se deben analizar las filas anteriores
		 * por eso k=i-1*/
		int k=i-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[hijo][k][j]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se suma para evaluar el nucleotido siguiente
		}
	}
	return contador;
}
//mutaciones de pirimidinas
int mut_columna(int lado, int i, int j, int hijo, char adn[hijo][lado][lado]){
	int contador, nucleotidos;
	if(adn[hijo][i][j]=='T'){
		/*se deben analizar las columnas siguientes
		 * por eso k=j+1*/
		int k=j+1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k<lado){
			if(nucleotidos<4){
				if(adn[hijo][i][k]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k++;//se suma para evaluar el nucleotido siguiente
		}
	}
	else if(adn[hijo][i][j]=='C'){
		/*se deben analizar las columnas anteriores
		 * por eso k=j-1*/
		int k=j-1;
		nucleotidos=0;
		contador=0;
		//se repite mientras k sea menor al largo de la matriz
		while(k>=0){
			if(nucleotidos<4){
				if(adn[hijo][i][k]==adn[hijo][i][j]){
					contador++;
				}
				nucleotidos++;
			}
			k--;//se suma para evaluar el nucleotido siguiente
		}
	}
	return contador;
}
//calcula lo correspondiente a la segunda mutación
int cuantos_alrededor(int lado, int i, int j, int hijo, char adn[hijo][lado][lado]){
	if(adn[hijo][i][j]=='A' || adn[hijo][i][j]=='G'){
		//retorna la mutacion de la fila debido a que son purinas
		return mut_fila(lado, i, j, hijo, adn);
	}
	else if(adn[hijo][i][j]=='C' || adn[hijo][i][j]=='T'){
		//retorna la mutacion de la columna debido a que son pirimidinas
		return mut_columna(lado, i, j, hijo, adn);
	}
	return 0;
}

//cuenta los vecinos
int cuantos_vecinos(char base, char vecino, int nvecino){
	/*se recibe la cantidad de vecinos que ya se tienen en cuenta
	 * y si el vecino nuevo que se evalúa es el correspondiente se retorna los vecinos recibidos+1*/
	switch(base){
		case 'A':
		if(vecino=='G'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'C':
		if(vecino=='T'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'G':
		if(vecino=='A'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
		case 'T':
		if(vecino=='C'){
			return nvecino+1;
		}
		else{
			return nvecino;
		}
		break;
	}
	return nvecino;
}
//funcion que realiza cambios
void mutaciones(int lado, int hijo, int generacion, char adn[hijo][lado][lado], int largo, char secuencia[largo]){
	//variables
	int vecinos[lado][lado], mutacion[lado][lado], vecinijillo;
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			/*contadores para evaluar la matriz respecto a la base (vecinos, columnas y filas)
			 * las de dos dimensiones son las que se mantienen con la respectiva letra
			 * mientras que el contador sin dimensiones es temporal para evitar errores*/
			vecinijillo=0;
			vecinos[i][j]=0;
			mutacion[i][j]=0;
			/*recorre los alrededores de la matriz (8 vecinos)*/
			for(int k=i-1; k<=i+1; k++){
				for(int l=j-1; l<=j+1; l++){
					/*en caso de que los valores sean negativos o mayor al largo de la matriz*/
					if((k>=0 && k<lado)&&(l>=0 && l<lado)){
						vecinos[i][j]=cuantos_vecinos(adn[hijo][i][j], adn[hijo][k][l], vecinijillo);
						vecinijillo=vecinos[i][j];
					}
					else{
					vecinijillo=vecinijillo;
					}	
				}
			}
			/*cálculos de frecuencia y la segunda mutacion
			 * cálculo de los alrededores de acuerdo a la segunda funcion*/
			mutacion[i][j]=cuantos_alrededor(lado, i, j, hijo, adn);
		}
	}
	//frecuencia de la matriz
	char frecuencia[lado*lado];
	//si la secuencia que está siendo mutadada es la 1, o más, se calcula la frecuencia
	if(hijo>=1){
		int k=0;
		for(int i=0; i<lado; i++){
			for(int j=0; j<lado; j++){
				//arreglo que contiene todas las bases de la secuencia
				frecuencia[k]=adn[hijo][i][j];
				k++;
			}
		}
	}
	/*mutaciones*/
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			adn[hijo+1][i][j]=letra_nueva(hijo, lado, adn, frecuencia, vecinos[i][j], mutacion[i][j], adn[hijo][i][j]);
		}
	}
	/*mientras la cantidad de hijos realzados sea menor a las generaciones pedidas*/
	if(hijo<generacion){
		//para enviar la recursividad con la siguiente generación
		hijo++;
		printf("Generacion %d: \n", hijo);
		imprimir_secuencia(lado, hijo, adn);
		//se llama la función con la generación mutada
		mutaciones(lado, hijo, generacion, adn, largo, secuencia);
	}
	else{
		analisis(lado, generacion, adn, largo, secuencia);
	}
}
//funcion que realiza la primera secuencia
void secuencia_molde(int lado, int generacion, int largo, char secuencia[largo]){
	srand(time(0));
	//variable de matriz
	char adn[generacion][lado][lado];
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			//asignacion de bases a aleatoriedad
			adn[0][i][j]="ACGT"[rand()%4];
		}
	}
	//imprimir secuencia molde
	printf("Generación 0: \n");
	imprimir_secuencia(lado, 0, adn);
	
	//realización de cambios
	mutaciones(lado, 0, generacion, adn, largo, secuencia);
}

//funcion que recibe la dimension
int dimensiones(){
	int dimension;
	
	do{
		printf("Por favor, ingrese la dimension de secuencia: ");
		scanf("%d", &dimension);
	}while (dimension<5);
	
	return dimension;
}
//función que recibe las generaciones
int generacion(){
	int generaciones;
	
	printf("Por favor, ingrese generaciones a analizar: ");
	scanf("%d", &generaciones);
	
	return generaciones;
}
void menu(int lado, int generaciones, int largo, char secuencia[largo]){
	int opcion;
	printf("Por favor, indique que desea hacer en este momento: ");
	printf("\n1. Cambiar la secuencia.\n2. Cambiar la dimensión.\n3. Cambiar las generaciones.\n4. Terminar el juego.\n");
	do{
		printf("Ingrese opción: ");
		scanf("%d", &opcion);
	}while(opcion<1 || opcion>4);
	switch(opcion){
		case 1:
		printf("Ingrese secuencia: ");
		scanf("%s", secuencia);
		largo=strlen(secuencia);
		printf("-%d-", largo);
		secuencia_molde(lado, generaciones, largo, secuencia);
		menu(lado, generaciones, largo, secuencia);
		break;
		case 2:
		lado=dimensiones();
		secuencia_molde(lado, generaciones, largo, secuencia);
		menu(lado, generaciones, largo, secuencia);
		break;
		case 3:
		generaciones=generacion();
		secuencia_molde(lado, generaciones, largo, secuencia);
		menu(lado, generaciones, largo, secuencia);
		break;
		case 4:
		printf("¡Gracias por jugar!\n pongame el 7\n");
		break;
	}
}
//funcion que realiza el programa
void realizar_matrices(){
	//declaracion de variables
	int dimension, generaciones, largo;
	
	printf("Se verá una secuencia molde y se realizaran mutaciones por generación. \n");
	
	//valorización de variableses mediante función
	dimension=dimensiones();
	generaciones=generacion();
	char secuencia[dimension*dimension];
	printf("Ingrese secuencia: ");
	scanf("%s", secuencia);
	largo=strlen(secuencia);
	
	//creacion de secuencias
	secuencia_molde(dimension, generaciones, largo, secuencia);
	menu(dimension, generaciones, largo, secuencia);
}
int main()
{
	//función que realiza el programa
	realizar_matrices();
	
	return 0;
}
