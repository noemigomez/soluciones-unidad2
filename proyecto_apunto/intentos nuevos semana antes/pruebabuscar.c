#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
void imprimir(int lado, char adn[lado][lado]){
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			printf("|%c|", adn[i][j]);
		}
		printf("\n");
	}
}
void imprimir_encuentros(int veces, int encontrado_i[veces], int encontrado_j[veces]){
	printf("\nEncontrada en: ");
	for(int k=0; k<veces; k++){
		printf("[%d, %d]", encontrado_i[k], encontrado_j[k]);
	}
}
void buscar_2(int lado, char adn[lado][lado], int i, int j, int largo, char secuencia[largo]){
	/*contador que cuenta cuantas veces se realiza el ciclo
	 * veces cuenta las veces que se encuentra la coincidencia*/
	int contador, veces;
	int encontrado_i[largo], encontrado_j[largo]; //coordenadas para imprimir
	contador=0;
	veces=0;
	while(contador<largo){
		//si j es mayor al largo de la matriz, se pasa a la fila siguiente
		if(j>=lado){
			j=j-lado;
			i++;
		}
		//si se encuentra, se guardan las variables y aumenta j para evaluar la siguiente
		if(secuencia[contador]==adn[i][j]){
			encontrado_i[veces]=i;
			encontrado_j[veces]=j;
			veces++;
			j++;
		}
		else{
			veces=0;
		}
		contador++;
	}
	//si se encuentra tantas veces como el largo, se encontró la secuencia
	if(veces==largo){
		imprimir_encuentros(veces, encontrado_i, encontrado_j);
	}
}
void vertical(int lado, char adn[lado][lado], int i, int j, int largo, char secuencia[largo]){
	int contador=0;
	int encontrada_i[largo], encontrada_j[largo];
	if(i+largo<=lado){
		for(int k=i; k<i+largo; k++){
			if(secuencia[contador]==adn[k][j]){
				encontrada_i[contador]=k;
				encontrada_j[contador]=j;
				contador++;
			}
		}
		if(contador==largo){
			imprimir_encuentros(contador, encontrada_i, encontrada_j);
			contador=0;
		}
	}
}
void segundadiagonal(int lado, char adn[lado][lado], int i, int j, int largo, char secuencia[largo]){
	int contador=0, encontrado_i[largo], encontrado_j[largo];
	for(int k=i; k<i+largo; k++){
		if(adn[k][lado-(k+1)]==secuencia[contador]){
			encontrado_i[contador]=k;
			encontrado_j[contador]=lado-(k+1);
			contador++;
		}
	}
	if(contador==largo){
		imprimir_encuentros(contador, encontrado_i, encontrado_j);
	}
}
//me cuenta dos veces la diagonal, pero la encuentra, no se porqué
void diagonal(int lado, char adn[lado][lado], int i, int j, int largo, char secuencia[largo]){
	int contador=0, encontrado_i[largo], encontrado_j[largo];
	for(int k=i; k<i+largo; k++){
		if(adn[k][k]==secuencia[contador]){
			encontrado_i[contador]=k;
			encontrado_j[contador]=k;
			contador++;
		}
	}
	if(contador==largo){
		imprimir_encuentros(contador, encontrado_i, encontrado_j);
	}
}
void busqueda(int lado, char adn[lado][lado], int largo, char secuencia[largo], int buscar){
	//~ for(int i=0; i<largo; i++){
		//~ printf("%c", secuencia[i]);
	//~ }
	for(int i=0; i<lado; i++){
		for(int j=0; j<lado; j++){
			if(secuencia[0]==adn[i][j]){
				buscar_2(lado, adn, i, j, largo, secuencia);
				vertical(lado, adn, i, j, largo, secuencia);
				if(i==j){
					diagonal(lado, adn, i, j, largo, secuencia);
				}
				else if(i+j==(lado-1)){
					segundadiagonal(lado, adn, i, j, largo, secuencia);
				}
			}
		}
	}
	if(buscar==0){
	char sec_nueva[largo];
	int b=largo-1;
	for(int a=0; a<largo; a++){
		sec_nueva[a]=secuencia[b];
		b--;
		printf("%c", sec_nueva[a]);
	}
	int cont=0;
	for(int c=0; c<largo; c++){
		if(sec_nueva[c]==secuencia[c]){
			cont++;
		}
	}
	if(cont!=largo){
		printf("\nEN REVERSA\n");
		busqueda(lado, adn, largo, sec_nueva, 1);
	}
}
}
void buscar_secuencia(int lado, char adn[lado][lado], char secuencia[100000]){
	//variables
	int largo;
	//calculo del largo de la secuencia ingresada
	largo=strlen(secuencia);
	//proceso para adecuar la secuencia a su largo para la búsqueda
	char subsecuencia[largo];
	int i=0;
	for(int j=0; j<largo; j++){
		subsecuencia[j]=secuencia[i];
		i++;
	}
	for(i=0; i<largo; i++){
		printf("%c", subsecuencia[i]);
	}
	//llamado a funcion que realiza busquedas
	busqueda(lado, adn, largo, subsecuencia, 0);
}

void crear_matriz(char secuencia[100000], int generaciones){
	int lado;
	srand(time(0));
	lado=(rand()%3)+5;
	
	char adn[lado][lado];
	
	for(int h=0; h<generaciones; h++){
		printf("\nGeneración %d.\n", h);
		for(int i=0; i<lado; i++){
			for(int j=0; j<lado; j++){
				adn[i][j]="ACGT"[rand()%4];
			}
		}
		imprimir(lado, adn);
		buscar_secuencia(lado, adn, secuencia);
	}
}

int main()
{
	char secuencia[100000];
	int generaciones;
	printf("Ingrese secuencia: ");
	scanf(" %[^\n]s", secuencia);
	printf("Ingrese generaciones: ");
	scanf("%d", &generaciones);
	
	crear_matriz(secuencia, generaciones);
	
	return 0;
}

