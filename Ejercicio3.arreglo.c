/*Sustituir todos los espacios en blanco de una frase por un asterisco*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int main()
{
char frase[100]= "esto es una frase con espacio";
	int largo;
	//variables
	
	printf ("A la frase ingresada le vamos a reemplazar los espacios por un *\n");
	printf ("Ingrese una frase\n");
	gets (frase);
	//usuario ingresa datos 

	largo = strlen(frase); //Para calcular el largo de la frase ingresada
	
	for (int i=0; i<largo; i++){ //Ciclo para que se muestra el * cada vez que hay un espacio
		if (frase[i]==' '){ 
			 frase[i] = '*';
		}
		  printf("%c", frase[i]); //se imprime la frase final con *
}      


	return 0; //retorna el programa
}

